#!/usr/bin/env python
# -*- coding:utf-8 -*-


class TimetableExporterError(Exception):
    def __init__(self, timetable_id):
        self.timetable_id = timetable_id

    def __str__(self):
        return repr(self.timetable_id)

#!/usr/bin/env python
# -*- coding:utf-8 -*-
u"""user defined error exception for Timetable
"""

class InvalidTimetableError(Exception):
    u"""user defined error exception for Timetable class
    """

    def __init__(self, timetable_id):
        Exception.__init__(self)
        self.timetable_id = timetable_id

    def __str__(self):
        return repr(self.timetable_id)

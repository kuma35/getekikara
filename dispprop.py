#!/usr/bin/env python
# -*- coding: utf-8 -*-
# display properties
from oudiaformatdefs import *

OUD_DISP_PROP = [u"DispProp" + OUD_BLOCK,
                 u"JikokuhyouFont=PointTextHeight=9;Facename=ＭＳ ゴシック",
                 u"DiaEkimeiFont=PointTextHeight=9;Facename=ＭＳ ゴシック",
                 u"DiaJikokuFont=PointTextHeight=9;Facename=ＭＳ ゴシック",
                 u"DiaRessyaFont=PointTextHeight=9;Facename=ＭＳ ゴシック",
                 u"CommentFont=PointTextHeight=9;Facename=ＭＳ ゴシック",
                 u"DiaMojiColor=00000000",
                 u"DiaHaikeiColor=00FFFFFF",
                 u"DiaRessyaColor=00000000",
                 u"DiaJikuColor=00C0C0C0",
                 u"EkimeiLength=6",
                 u"JikokuhyouRessyaWidth=5",
                 OUD_BLOCKEND,
                 ]


class DispProp(object):
    def __init__(self, logger, props=None):
        self.logger = logger       # logging object
        if props:
            self.props = props
        else:
            self.props = OUD_DISP_PROP

    def _get_disp_prop_list(self) :
        return self.props

    disp_prop_list = property(_get_disp_prop_list)

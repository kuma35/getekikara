#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""traingradeのテスト"""
import traingrade
import gettimetablelog


def test_get_grade_code():
    u"""取り込んだデータと取得できるコードが一致しているかどうかテスト"""
    grade = traingrade.TrainGrade(logger=gettimetablelog.logging.getLogger("gettimetable"))
    assert grade.get_code(u"普通") == 0
    assert grade.get_code(u"快速") == 1
    assert grade.get_code(u"急行") == 2
    assert grade.get_code(u"特急") == 3
    assert grade.get_code(u"快特") == 4
    assert grade.get_code(u"準急") == 5
    assert grade.get_code(u"新幹線のぞみ") == 6
    assert grade.get_code(u"新幹線ひかり") == 7
    assert grade.get_code(u"新幹線こだま") == 8
    assert grade.get_code(u"新幹線つばめ") == 9
    assert grade.get_code(u"新幹線やまびこ") == 10
    assert grade.get_code(u"新幹線つばさ") == 11
    assert grade.get_code(u"新幹線はやて") == 12
    assert grade.get_code(u"新幹線Ｍａｘやまびこ") == 13
    assert grade.get_code(u"新幹線なすの") == 14
    assert grade.get_code(u"新幹線とき") == 15
    assert grade.get_code(u"新幹線たにがわ") == 16
    assert grade.get_code(u"新幹線Ｍａｘとき") == 17
    assert grade.get_code(u"新幹線あさま") == 18
    assert grade.get_code(u"ライナー") == 19
    assert grade.get_code(u"新幹線はやぶさ") == 20
    assert grade.get_code(u"") == 0
    assert grade.get_code(u"大和路") == 0

def test_get_grade_list():
    u"""取得したリストとメソッドから取得したリストが一致するかどうかテスト"""
    grade = traingrade.TrainGrade(logger=gettimetablelog.logging.getLogger("gettimetable"))
    assert traingrade.OUD_TRAIN_GRADE == grade.grade_list_formatted

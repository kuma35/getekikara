#!/usr/bin/env python_
# -*- coding: utf-8 -*-
# LoopLine.py
# 2011/05/26 EkikaraDb.pyより分離
u"""ループになっている路線の処理"""

import sqlite3
from ekikaradb import Timetable

SELECT_TRAIN_TIME_LIST_SQL = u"""
select timetable_id,
       train_number,
       train_order,
       url,
       station_order,
       station_name
from train_time
where timetable_id =? and train_number = ? and train_order = ?
order by station_order
"""


SELECT_DOWN_EKI_TRAIN_STOP_SQL = u"""
select e.eki_order, t.station_order
from oudia_eki e left join train_time t on e.eki_name = t.station_name
where t.timetable_id = ? and
      t.train_number = ? and
      t.train_order = ? and
      t.staton_order = ?
"""

SELECT_UP_EKI_TRAIN_STOP_SQL = u"""
select e.eki_order, t.station_order
from oudia_eki e left join train_time t on e.eki_name = t.station_name
where t.timetable_id = ? and
      t.train_number = ? and
      t.train_order = ? and
      t.station_order = ?
"""

SELECT_TRUE_LINE_STOP_SQL = u"""
select e.eki_order,
       e.eki_name,
       s.station_order,
       s.station_name,
       s.train_arrival,
       s.train_departure
from (oudia_eki e left join true_train_stop t on
      e.eki_order = t.eki_order) inner join train_time s
      on t.timetable_id = s.timetable_id and
         t.train_number = s.train_number and
         t.train_order = s.train_order and
         t.station_order = s.station_order
where t.timetable_id = ? and t.train_number = ? and t.train_order = ?
order by e.eki_order
"""

COUNT_EXIST_EKI_SQL = u"""
select count(eki_order) cnt
from oudia_eki
where eki_name = ?
"""

COUNT_EKI_SQL = u"""
select eki_name, count(eki_name) cnt
from oudia_eki
group by eki_name
order by eki_name
"""


CHECK_OUDIA_EKI_EXPROLE_FORWARD_SQL = u"""
select count(eki_order) cnt
from oudia_eki
where eki_order > ?
"""

CHECK_OUDIA_EKI_EXPROLE_FORWARD_UP_SQL = u"""
select count(eki_order) cnt
from oudia_eki
where eki_order < ?
"""

CHECK_OUDIA_EKI_EXPROLE_BACKWARD_SQL = u"""
select count(eki_order) cnt
from oudia_eki
where eki_order < ?
"""

CHECK_OUDIA_EKI_EXPROLE_BACKWARD_UP_SQL = u"""
select count(eki_order) cnt
from oudia_eki
where eki_order > ?
"""

SELECT_OUDIA_EKI_EXPROLE_FORWARD_SQL = u"""
select eki_order,
       eki_name
from oudia_eki
where eki_order > ? and
      eki_name = ?
order by eki_order asc
"""

SELECT_OUDIA_EKI_EXPROLE_FORWARD_UP_SQL = u"""
select eki_order,
       eki_name
from oudia_eki
where eki_order < ? and
      eki_name = ?
order by eki_order desc
"""

SELECT_OUDIA_EKI_EXPROLE_BACKWARD_SQL = u"""
select eki_order,
       eki_name
from oudia_eki
where eki_order < ? and
      eki_name = ?
order by eki_order desc
"""

SELECT_OUDIA_EKI_EXPROLE_BACKWARD_UP_SQL = u"""
select eki_order,
       eki_name
from oudia_eki
where eki_order > ? and
      eki_name = ?
order by eki_order asc
"""

CHECK_TRAIN_TIME_EXPROLE_FORWARD_SQL = u"""
select count(station_order) cnt
from train_time
where timetable_id = ? and
      train_number = ? and
      train_order = ? and
      station_order > ?
"""

CHECK_TRAIN_TIME_EXPROLE_BACKWARD_SQL = u"""
select count(station_order) cnt
from train_time
where timetable_id = ? and
      train_number = ? and
      train_order = ? and
      station_order < ?
"""

SELECT_TRAIN_TIME_EXPROLE_FORWARD_SQL = u"""
select station_order, station_name
from train_time
where timetable_id = ? and
      train_number = ? and
      train_order = ? and
      station_order > ?
order by station_order asc
"""

SELECT_TRAIN_TIME_EXPROLE_BACKWARD_SQL = u"""
select station_order, station_name
from train_time
where timetable_id = ? and
      train_number = ? and
      train_order = ? and
      station_order < ?
order by station_order desc
"""


class ExproleList(object):
    u"""路線駅探索リスト＆列車停車駅探索リストのスーパークラス

    路線駅リストより導出した路線駅探索リスト、
    列車停車駅駅リストより導出した列車停車駅探索リストを
    保持するオブジェクトを生成するクラス。
    """
    def __init__(self, db_obj, target_order):
        self.db = db_obj
        self.target_order = target_order
        self.forwardable_flag = False  # 前方へ探索可能かどうか
        self.backwardable_flag = False  # 後方へ探索可能かどうか
        self.last_forward_station_order = target_order   # 最後に探索したid
        self.last_backward_station_order = target_order  # 最後に探索したid

    @property
    def forwardable(self):
        u"""下り方向へ探索可能かどうかを返します。"""
        return self.forwardable_flag

    @property
    def backwardable(self):
        u"""上り方向へ探索可能かどうかを返します。"""
        return self.backwardable_flag

    @property
    def target_station_order(self):
        u"""現在調査対象となっている駅を返します。"""
        return self.target_order


class LineStationExprole(ExproleList):
    u"""路線駅探索リスト

    oudaia_ekiテーブルを対象とする。
    """
    def __init__(self, db_obj, target_order, up_down='down'):
        ExproleList.__init__(self, db_obj, target_order)
        if up_down == 'up':
            self.up_down = 'up'
        else:
            self.up_down = 'down'       # よくわからんのは全部downにしとく。
        # forward方向へ探索可能かどうか調べ、self.forwardOKフラグを設定。
        if self.up_down == 'up':
            rows = self.db.conn.execute(CHECK_OUDIA_EKI_EXPROLE_FORWARD_UP_SQL,
                                        (self.target_order, ))  # one tupple
        else:
            rows = self.db.conn.execute(CHECK_OUDIA_EKI_EXPROLE_FORWARD_SQL,
                                        (self.target_order, ))  # one tupple
        row = rows.fetchone()
        if row['cnt'] is not None:
            self.forwardable_flag = True
        rows.close()
        # backward方向へ探索可能かどうか調べ、self.backwardable_flagフラグを設定。
        if self.up_down == 'up':
            rows = self.db.conn.execute(CHECK_OUDIA_EKI_EXPROLE_BACKWARD_UP_SQL,
                                        (self.target_order, ))
        else:
            rows = self.db.conn.execute(CHECK_OUDIA_EKI_EXPROLE_BACKWARD_SQL,
                                        (self.target_order, ))
        row = rows.fetchone()
        if row['cnt'] is not None:
            self.backwardable_flag = True
        rows.close()

    def searchForward(self, name):
        u"""前方へ駅名nameで検索し、見つかればそのeki_orderを返す。"""
        result = None
        if self.last_forward_station_order is not None:
            if self.up_down == 'up':
                rows = self.db.conn.execute(
                    SELECT_OUDIA_EKI_EXPROLE_FORWARD_UP_SQL,
                    (self.last_forward_station_order, name))
            else:
                rows = self.db.conn.execute(
                    SELECT_OUDIA_EKI_EXPROLE_FORWARD_SQL,
                    (self.last_forward_station_order, name))
            row = rows.fetchone()
            if row is not None:
                self.last_forward_station_order = row['eki_order']
                result = row['eki_name']
            else:
                self.last_forward_station_order = None
            rows.close()
        return result

    def searchBackward(self, name):
        u"""後方(desc)方向へnameで探索し、見つかったらeki_orderを返す。"""
        result = None
        if self.last_backward_station_order is not None:
            if self.up_down == 'up':
                rows = self.db.conn.execute(
                    SELECT_OUDIA_EKI_EXPROLE_BACKWARD_UP_SQL,
                    (self.last_backward_station_order, name))
            else:
                rows = self.db.conn.execute(
                    SELECT_OUDIA_EKI_EXPROLE_BACKWARD_SQL,
                    (self.last_backward_station_order, name))
            row = rows.fetchone()
            if row is not None:
                self.last_backward_station_order = row['eki_order']
                result = row['eki_name']
            else:
                self.last_backward_station_order = None
        return result


class TrainStationExprole(ExproleList):
    u"""列車停車駅探索リスト"""
    def __init__(self,
                 db_obj,
                 timetable_id,
                 train_number,
                 train_order,
                 target_order):
        ExproleList.__init__(self, db_obj, target_order)
        self.timetable_id = timetable_id
        self.train_number = train_number
        self.train_order = train_order
        # 'up'以外は全てdownとみなす
        # forward方向へ探索可能か調べforwardable_flagフラグを設定。
        rows = self.db.conn.execute(CHECK_TRAIN_TIME_EXPROLE_FORWARD_SQL,
                                    (self.timetable_id,
                                     self.train_number,
                                     self.train_order,
                                     self.target_order))
        row = rows.fetchone()
        if row['cnt'] > 0:
            self.forwardable_flag = True
        rows.close()

        # backward方向へ探索可能か調べbackwardable_flagフラグを設定。
        rows = self.db.conn.execute(CHECK_TRAIN_TIME_EXPROLE_BACKWARD_SQL,
                                    (self.timetable_id,
                                     self.train_number,
                                     self.train_order,
                                     self.target_order))
        row = rows.fetchone()
        if row['cnt'] > 0:
            self.backwardable_flag = True
        rows.close()
        self.forwardCursor = self.db.conn.execute(
            SELECT_TRAIN_TIME_EXPROLE_FORWARD_SQL,
            (self.timetable_id,
             self.train_number,
             self.train_order,
             self.last_forward_station_order))
        self.backwardCursor = self.db.conn.execute(
            SELECT_TRAIN_TIME_EXPROLE_BACKWARD_SQL,
            (self.timetable_id,
             self.train_number,
             self.train_order,
             self.last_backward_station_order))

    def __del__(self):
        try:
            self.forwardCursor.close()
        finally:
            pass
        try:
            self.backwardCursor.close()
        finally:
            pass

    def fetchForward(self):
        u"""前方(order by asc)方向へ一歩進み、その駅名を返す
        """
        result = None
        if self.last_forward_station_order is not None:
            row = self.forwardCursor.fetchone()
            if row is not None:
                self.last_forward_station_order = row['station_order']
                result = row['station_name']
            else:
                self.last_forward_station_order = None
                self.forwardCursor.close()
        return result

    def fetchBackward(self):
        u"""後方(order by desc)方向へ一歩進み、駅名を返す。
        """
        result = None
        if self.last_backward_station_order is not None:
            row = self.backwardCursor.fetchone()
            if row is not None:
                self.last_backward_station_order = row['station_order']
                result = row['station_name']
            else:
                self.last_backward_station_order = None
                self.backwardCursor.close()
        return result


class TimetableLoopLine(Timetable):
    u"""

    override setToOudiaEki
    override ekiTimeCursor
    """
    def _setCntEki(self):
        u"""oudia_ekiから駅名ごとの出現数をリストにして取得"""
        try:
            self._cntEki = dict(
                [(row['eki_name'], row['cnt'])
                 for row in self.db.conn.execute(COUNT_EKI_SQL)])
        except sqlite3.Error, e:
            self.logger.debug(u"COUNT_EKI_SQL;An error occurred: %s" % e)

    def __init__(self, db, target_timetable_id, logger, isCheckOrder):
        Timetable.__init__(self, db, target_timetable_id, logger)
        #oudia_ekiの重複数をあらかじめ求めておく。
        #self._cntEkiExecCnt = 0
        self._cntEki = {}
        self.isCheckOrder = isCheckOrder

    def getCntEki(self, name):
        u"""指定の駅名の駅がoudia_eki内に幾つあるかを返す

        同じ駅名が2以上ならループ線の可能性がある
        __init__()内だと失敗するのでここでoudia_ekiから取得
        """
        while len(self._cntEki) <= 0:
            self._setCntEki()
        if name in self._cntEki:
            return self._cntEki[name]
        else:
            return 0

    def trainTimeList(self, train_number, train_order):
        u"""検索のsrc側となるtrain_timeのリスト"""
        self.logger.debug((u"target_timetable=%d,"
                           u"trainTimeList(train_number=%s,train_order=%d)") %
                          (self.target_timetable, train_number, train_order))
        return self.db.conn.execute(
            SELECT_TRAIN_TIME_LIST_SQL,
            (self.target_timetable, train_number, train_order))

    def countExistEki(self, ekiName):
        u"""指定の駅名の駅がoudia_ekiに幾つあるか返す。"""
        return self.db.conn.execute(COUNT_EXIST_EKI_SQL,
                                    (ekiName, ))  # one element tuple

    def pick(self,
             train_number,
             train_order,
             trainStop,
             lastOrder=None,
             updown='down'):
        u"""路線駅と列車停車駅のマッチング結果を返す
        前提:
        路線駅リストに省略が無い前提で探索する。
        列車停車駅リストの方は通過駅含めすべての駅が掲載されているため、
        路線駅リストに省略があると探索に失敗する。
        路線駅リストを省略形にするのは探索終了後に行う。
        oudia_ekiとマッチする駅名が2つ以上の場合のみここに来る。
        パターン:
        ( 1) [A, , , , , , , , , ] and [B, , , , , , , , , ] 探索:→
        ( 2) [A, , , , , , , , , ] and [ , , , ,B, , , , , ] 探索:→
        ( 3) [A, , , , , , , , , ] and [ , , , , , , , , ,B] 探索:不可
        ( 4) [ , , , ,A, , , , , ] and [B, , , , , , , , , ] 探索:→
        ( 5) [ , , , ,A, , , , , ] and [ , , , ,B, , , , , ] 探索:←→
        ( 6) [ , , , ,A, , , , , ] and [ , , , , , , , , ,B] 探索:←
        ( 7) [ , , , , , , , , ,A] and [B, , , , , , , , , ] 探索:不可
        ( 8) [ , , , , , , , , ,A] and [ , , , ,B, , , , , ] 探索:←
        ( 9) [ , , , , , , , , ,A] and [ , , , , , , , , ,B] 探索:←
        (10) [A]                   and 任意                  探索:不可
        (11) 任意                  and [B]                   探索:不可
        """
        attr = self.getAttr()
        # 列車停車駅と組み合わせた時の探索パターン
        srcExprole = TrainStationExprole(self.db,
                                         self.target_timetable,
                                         train_number,
                                         train_order,
                                         trainStop['station_order'])
        if (not srcExprole.forwardable) and (not srcExprole.backwardable):
            return None  # 探索不可(上記パターン10)
        dstExproleList = []
        for eki in self.existEki(trainStop['station_name'],
                                 lastOrder,
                                 updown):
            isForward = False
            isBackward = False
            dstExprole = LineStationExprole(self.db,
                                            eki['eki_order'],
                                            attr['updown'])
            if srcExprole.forwardable and srcExprole.backwardable:
                # パターン 4～6
                if dstExprole.forwardable and dstExprole.backwardable:
                    # パターン5
                    isForward = True
                    isBackward = True
                elif dstExprole.forwardable:
                    # パターン 4
                    isForward = True
                elif dstExprole.backwardable:
                    # パターン 6
                    isBackward = True
                else:
                    pass  # both False ... see パターン 11
            elif srcExprole.forwardable:
                # パターン 1～3
                if dstExprole.forwardable and dstExprole.backwardable:
                    # パターン2
                    isForward = True
                elif dstExprole.forwardable:
                    # パターン1
                    isForward = True
                elif dstExprole.backwardable:
                    pass  # パターン3
                else:
                    pass  # both False ... see パターン 11
            elif srcExprole.backwardable:
                # パターン7～9
                if dstExprole.forwardable and dstExprole.backwardable:
                    # パターン8
                    isBackward = True
                elif dstExprole.forwardable:
                    # パターン7
                    pass
                elif dstExprole.backwardable:
                    # パターン9
                    isBackward = True
                else:
                    pass  # both False ... see パターン 11
            else:
                pass  # both false
            if isForward or isBackward:
                dstExproleList.append({'exprole': dstExprole,
                                       'pattern': {'forward':  isForward,
                                                   'backward': isBackward, }})
        self.logger.debug(Timetable.pretty.pformat(dstExproleList))
        while len(dstExproleList) > 1:
            for dstIndex in range(len(dstExproleList) - 1, -1, -1):
                # 逆順。途中で削除が入ることがあるため
                if dstExproleList[dstIndex]['pattern']['forward']:
                    forwardStationName = srcExprole.fetchForward()
                    if dstExproleList[dstIndex]['exprole'].searchForward(
                        forwardStationName) is None:
                        # この駅を取り除く(popした値は捨てる
                        dstExproleList.pop(dstIndex)
                        if len(dstExproleList) <= 1:
                            break
                        continue
                if dstExproleList[dstIndex]['pattern']['backward']:
                    backwardStationName = srcExprole.fetchBackward()
                    if dstExproleList[dstIndex]['exprole'].searchBackward(
                        backwardStationName) is None:
                        # この駅を取り除く(popした値は捨てる
                        dstExproleList.pop(dstIndex)
                        if len(dstExproleList) <= 1:
                            break
                        continue
        self.logger.debug(Timetable.pretty.pformat(dstExproleList))
        if len(dstExproleList) == 1:
            # eki_order を返す
            return dstExproleList[0]['exprole'].target_station_order
        else:
            return None  # 見つからなかった!!

    def prepareEkiTimeCursor(self,
                             train_number,
                             train_order,
                             updown):  # 'down' or 'up'
        u"""列車停車駅で路線駅リストにマッチするのだけtrue_train_stopにセット

        ----
        機能
        ----

        列車停車駅で路線駅リストにマッチするのだけ
        true_train_stop
        にセットする

        ------
        戻り値
        ------

        マッチングした数を返す
        """
        matchCount = 0    # マッチした駅数
        self.logger.debug((u"prepareEkiTimeCursor("
                           u"train_number=%s,train_order=%d)") %
                          (train_number, train_order))
        trainStopList = [
                {'timetable_id': row['timetable_id'],
                 'train_number': row['train_number'],
                 'train_order': row['train_order'],
                 'url': row['url'],
                 'station_order': row['station_order'],
                 'station_name': row['station_name'], }
                for row in self.trainTimeList(train_number, train_order) ]
        self.logger.debug(u"prepareEkiTimeCursor;len(trainStopList)=%d" %
                          (len(trainStopList),))
        try:
            lastOrder = None    # 直前のeki_order
            for trainStop in trainStopList:
                pair = None
                self.logger.debug(u"trainStop={'"
                                  u"train_number':%s,"
                                  u"'station_order':%d,"
                                  u"'station_name':%s }" %
                                  (trainStop['train_number'],
                                   trainStop['station_order'],
                                   trainStop['station_name'],))
                numEki = self.getCntEki(trainStop['station_name'])
                self.logger.debug(u"prepareEkiTimeCursor;numEki=%d" %
                                  (numEki,))
                if numEki == 1:
                    # 高速化(?)のため、というか旧バージョンはこれなので、
                    # 候補が1つだけのばあいは従来どおり単純にマッチングする。
                    # 厳密に言うとたとえユニークであってもそれは路線駅リストが
                    # たまたま一部分を切り出しただけの可能性があるので
                    #全部Loop対応チェックをした方が良いのかもしれない。
                    self.logger.debug(u"prepareEkiTimeCursor; "
                                      u"pair == 1 then "
                                      u"isExistEki(trainStop['station_name'])")
                    pair = self.isExistEki(trainStop['station_name'],
                                           lastOrder,
                                           updown)
                elif numEki > 1:
                    self.logger.debug(u"pair > 1 then pick()")
                    pair = self.pick(
                        train_number,
                        train_order,
                        trainStop,
                        lastOrder,
                        updown)
                else:  # 0
                    self.logger.debug(u"pair is no match!!")
                    continue  # no match

                # set result to true_train_stop table.
                if pair is not None:
                    matchCount += 1
                    self.logger.debug(u"pair=%d" % (pair))
                    try:
                        self.setTrueTrainStop(train_number,
                                              train_order,
                                              trainStop['station_order'],
                                              pair,
                                              by_commit=False)
                        lastOrder = pair  # 直前のeki_orderを更新
                    except sqlite3.Error, e:
                        self.logger.debug(u"setTrueTrainStop;error :%s" % e)
                else:
                    # unknown train stop!!
                    # 路線時刻表に駅記載が無い場合または、
                    # 当該列車停車駅が路線範囲外の場合は問題無い。
                    # ということで レポートするにしてもnoticeに留める。
                    self.logger.debug(u"pair=None,trainStop={'"
                                      u"train_number':%s,"
                                      u"'station_order':%d,"
                                      u"'station_name':%s }" %
                                      (trainStop['train_number'],
                                       trainStop['station_order'],
                                       trainStop['station_name'],))
        except sqlite3.Error, err_obj:
            self.logger.debug(u"prepareEkiTimeCusor trainStopList loop: %s" %
                              err_obj)
        try:
            self.db.conn.commit()  # commit seTrueTrainStop
        except sqlite3.Error, err_obj:
            self.logger.debug(u"commit setTrueTrainStop:%s" % err_obj)
        return matchCount

    def ekiTimeCursor(self, train_number, train_order):
        self.logger.debug(u"target_timetable=%d,"
                          u"ekiTimeCursor(train_number=%s, train_order=%s)" %
                          (self.target_timetable, train_number, train_order))
        return self.db.conn.execute(
            SELECT_TRUE_LINE_STOP_SQL,
            (self.target_timetable, train_number, train_order))

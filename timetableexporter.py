#!/usr/bin/env python
# -*- coding: utf-8 -*-
# output. data from DB.
import sys
import pprint
import locale


class TimetableExporter(object):
    u"""super class for output.
    """
    def __init__(self,
                 db,
                 timetable_id,
                 logger,
                 argv,          # command line ( for dia data property)
                 encode=None,   # dia data output encode ( not console)
                 newline=None,  # file newline. ex. oudia is CRLF.
                 status_option=False,
                 console_encode=locale.getpreferredencoding()):  # for massage to console
        self.pp = pprint.PrettyPrinter(indent=2)
        self.db = db    # ekikaradb
        self.target_id = timetable_id      # Timetable class object
        self.argv = argv
        self.logger = logger    # logging object
        if encode:
            self.encode = encode
        else:
            self.encode = None
        if newline:
            self.newline = newline
        else:
            self.newline = None
        self.console_encode = console_encode
        self.status_option = status_option

    def _writeln(self, fout, msg):
        u"""改行付きでfoutへ出力する。

        encode()はfoutオープン時に指定しているのでここでは指定していない。
        """
        fout.write(msg + self.newline)

    def _write(self, fout, msg):
        u"""改行無しでfoutへ出力する。

        encode()はfoutオープン時に指定しているのでここでは指定していない。
        """
        fout.write(msg)

    def _statusMsg(self, msg):
        if self.status_option:
            sys.stderr.write(msg.encode(self.console_encode))

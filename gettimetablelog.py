#!/usr/bin/env python
# -*- coding:utf-8 -*-
u"""ログ設定ファイル取り込み等
"""
import logging
import logging.config
import os.path


class NullHandler(logging.Handler):
    """ nothing output handler

    from Python 2.X document, Library reference.
    """
    def emit(self, record):
        pass


LOGGING_CONF = 'gettimetable.logging.conf'  # 今回使用する設定ファイルパスを指定します。
if os.path.exists(LOGGING_CONF):
    logging.config.fileConfig(LOGGING_CONF)  # 設定ファイルをセットします。
else:
    # nothing config file. minimum loging
    # set root logger, output to stderr, over error.
    logging.basicConfig(level=logging.ERROR)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import locale
CONSOLE_ENCODE = locale.getpreferredencoding()

import sys
import pageopener
import unittest
import gettimetablelog

class TestSequenceFunctions(unittest.TestCase) :
    def setUp(self) :
        self.logger = gettimetablelog.logging.getLogger("gettimetable")

    def tearDown(self) :
        pass

    def testNoOption(self) :
        pageOpener = pageopener.PageOpener(logger=self.logger)
        html = pageOpener.open("http://www.ekikara.jp/newdata/line/1301601/down1_1.htm")
        print len(html)


    def testTimeout(self) :
        pageOpener = pageopener.PageOpener(logger=self.logger,
                                           timeout=30,
                                           retryMax=10)
        html = None
        for i in range(0,10) :
            html = pageOpener.open("http://www.ekikara.jp/newdata/line/1301601/down1_1.htm")
            print len(html)

if __name__ == "__main__" :
    unittest.main()

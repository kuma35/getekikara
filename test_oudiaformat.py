#!/usr/bin/env python
# -*- coding: utf-8 -*-
u"""OudiaFormatのテスト"""

import locale
CONSOLE_ENCODE = locale.getpreferredencoding()

import codecs
import ekikaradb
import unittest
import gettimetablelog
import os
import oudiaformat
import sys

ATTR = [{'company' : u"JR",
         'railway_line' : u"飯田線",
         'updown' : u"down",
         'day' : u"平日",
         'direction' : u"天竜峡方面",
         },
        {'company' : u"JR",
         'railway_line' : u"飯田線",
         'updown' : u"up",
         'day' : u"平日",
         'direction' : u"豊橋方面",
         },]
STATION_LIST_1 = [
    {'name' : u"豊橋", 'arrival' : False, 'departure' : True},
    {'name' : u"船町", 'arrival' : False, 'departure' : True},
    {'name' : u"下地", 'arrival' : False, 'departure' : True},
    {'name' : u"小坂井", 'arrival' : False, 'departure' : True},
    {'name' : u"牛久保", 'arrival' : False, 'departure' : True},
    {'name' : u"豊川", 'arrival' : True, 'departure' : True},
    ]
STATION_LIST_2 = [
    {'name' : u"豊橋", 'arrival' : False, 'departure' : True},
    {'name' : u"船町", 'arrival' : False, 'departure' : True},
    {'name' : u"下地", 'arrival' : False, 'departure' : True},
    {'name' : u"小坂井", 'arrival' : False, 'departure' : True},
    {'name' : u"牛久保", 'arrival' : False, 'departure' : True},
    {'name' : u"豊川", 'arrival' : True, 'departure' : True},
    {'name' : u"三河一宮", 'arrival' : False, 'departure' :True },
    {'name' : u"長山", 'arrival' : True, 'departure' :True },
    {'name' : u"江島", 'arrival' : False, 'departure' :True },
    {'name' : u"東上", 'arrival' : False, 'departure' :True },
    {'name' : u"野田城", 'arrival' : False, 'departure' :True },
    {'name' : u"新城", 'arrival' : True, 'departure' :True },
    {'name' : u"東新町", 'arrival' : False, 'departure' :True },
    {'name' : u"茶臼山", 'arrival' : False, 'departure' :True },
    {'name' : u"三河東郷", 'arrival' : False, 'departure' :True },
    {'name' : u"大海", 'arrival' : False, 'departure' :True },
    {'name' : u"鳥居", 'arrival' : False, 'departure' :True },
    {'name' : u"長篠城", 'arrival' : False, 'departure' :True },
    {'name' : u"本長篠", 'arrival' : True, 'departure' :True },
    {'name' : u"三河大野", 'arrival' : False, 'departure' :True },
    {'name' : u"湯谷温泉", 'arrival' : False, 'departure' :True },
    {'name' : u"三河槙原", 'arrival' : False, 'departure' :True },
    {'name' : u"柿平", 'arrival' : False, 'departure' :True },
    {'name' : u"三河川合", 'arrival' : False, 'departure' :True },
    {'name' : u"池場", 'arrival' : False, 'departure' :True },
    {'name' : u"東栄", 'arrival' : False, 'departure' :True },
    {'name' : u"出馬", 'arrival' : False, 'departure' :True },
    {'name' : u"上市場", 'arrival' : False, 'departure' :True },
    {'name' : u"浦川", 'arrival' : False, 'departure' :True },
    {'name' : u"早瀬", 'arrival' : False, 'departure' :True },
    {'name' : u"下川合", 'arrival' : False, 'departure' :True },
    {'name' : u"中部天竜", 'arrival' : True, 'departure' :True },
    {'name' : u"佐久間", 'arrival' : False, 'departure' :True },
    {'name' : u"相月", 'arrival' : False, 'departure' :True },
    {'name' : u"城西", 'arrival' : False, 'departure' :True },
    {'name' : u"向市場", 'arrival' : False, 'departure' :True },
    {'name' : u"水窪", 'arrival' : True, 'departure' :True },
    {'name' : u"大嵐", 'arrival' : False, 'departure' :True },
    {'name' : u"小和田", 'arrival' : False, 'departure' :True },
    {'name' : u"中井侍", 'arrival' : False, 'departure' :True },
    {'name' : u"伊那小沢", 'arrival' : False, 'departure' :True },
    {'name' : u"鶯巣", 'arrival' : False, 'departure' :True },
    {'name' : u"平岡", 'arrival' : True, 'departure' :True },
    {'name' : u"為栗", 'arrival' : False, 'departure' :True },
    {'name' : u"温田", 'arrival' : False, 'departure' :True },
    {'name' : u"田本", 'arrival' : False, 'departure' :True },
    {'name' : u"門島", 'arrival' : False, 'departure' :True },
    {'name' : u"唐笠", 'arrival' : False, 'departure' :True },
    {'name' : u"金野", 'arrival' : False, 'departure' :True },
    {'name' : u"千代", 'arrival' : False, 'departure' :True },
    {'name' : u"天竜峡", 'arrival' : True, 'departure' :False },
    ]
STATION_LIST_3 = [
    {'name' : u"天竜峡", 'arrival' : False, 'departure' :True },
    {'name' : u"川路", 'arrival' : False, 'departure' :True },
    {'name' : u"時又", 'arrival' : False, 'departure' :True },
    {'name' : u"駄科", 'arrival' : False, 'departure' :True },
    {'name' : u"毛賀", 'arrival' : False, 'departure' :True },
    {'name' : u"伊那八幡", 'arrival' : False, 'departure' :True },
    {'name' : u"下山村", 'arrival' : False, 'departure' :True },
    {'name' : u"鼎", 'arrival' : False, 'departure' :True },
    {'name' : u"切石", 'arrival' : False, 'departure' :True },
    {'name' : u"飯田", 'arrival' : True, 'departure' :True },
    {'name' : u"桜町", 'arrival' : False, 'departure' :True },
    {'name' : u"伊那上郷", 'arrival' : False, 'departure' :True },
    {'name' : u"元善光寺", 'arrival' : False, 'departure' :True },
    {'name' : u"下市田", 'arrival' : False, 'departure' :True },
    {'name' : u"市田", 'arrival' : False, 'departure' :True },
    {'name' : u"下平", 'arrival' : False, 'departure' :True },
    {'name' : u"山吹", 'arrival' : False, 'departure' :True },
    {'name' : u"伊那大島", 'arrival' : True, 'departure' :True },
    {'name' : u"上片桐", 'arrival' : False, 'departure' :True },
    {'name' : u"伊那田島", 'arrival' : False, 'departure' :True },
    {'name' : u"高遠原", 'arrival' : False, 'departure' :True },
    {'name' : u"七久保", 'arrival' : False, 'departure' :True },
    {'name' : u"伊那本郷", 'arrival' : False, 'departure' :True },
    {'name' : u"飯島", 'arrival' : False, 'departure' :True },
    {'name' : u"田切", 'arrival' : False, 'departure' :True },
    {'name' : u"伊那福岡", 'arrival' : False, 'departure' :True },
    {'name' : u"小町屋", 'arrival' : False, 'departure' :True },
    {'name' : u"駒ヶ根", 'arrival' : True, 'departure' :True },
    {'name' : u"大田切", 'arrival' : False, 'departure' :True },
    {'name' : u"宮田", 'arrival' : False, 'departure' :True },
    {'name' : u"赤木", 'arrival' : False, 'departure' :True },
    {'name' : u"沢渡", 'arrival' : False, 'departure' :True },
    {'name' : u"下島", 'arrival' : False, 'departure' :True },
    {'name' : u"伊那市", 'arrival' : False, 'departure' :True },
    {'name' : u"伊那北", 'arrival' : False, 'departure' :True },
    {'name' : u"田畑", 'arrival' : False, 'departure' :True },
    {'name' : u"北殿", 'arrival' : False, 'departure' :True },
    {'name' : u"木ノ下", 'arrival' : False, 'departure' :True },
    {'name' : u"伊那松島", 'arrival' : True, 'departure' :True },
    {'name' : u"沢", 'arrival' : False, 'departure' :True },
    {'name' : u"羽場", 'arrival' : False, 'departure' :True },
    {'name' : u"伊那新町", 'arrival' : False, 'departure' :True },
    {'name' : u"宮木", 'arrival' : False, 'departure' :True },
    {'name' : u"辰野", 'arrival' : True, 'departure' :False },
    ]
TRAIN_1 = {
    'name' : None,
    'number' : u"519M",
    'train_order': 0,
    'type' : u"普通",
    'note' : (u"[駅着発時刻変更案内]"
              u"９月２０日　岡谷〜上諏訪　時刻･番線変更あり"
              u"（以下は通常運転の場合） ,"
              u"９月４日　岡谷〜上諏訪　時刻･番線変更あり"
              u"（以下は通常運転の場合）"),
    'gou' : None,
    'url' : u"http://www.ekikara.jp/newdata/detail/2301071/62965.htm",
    'stations' : [
        {'name' : u"豊橋", 'arrival' : None , 'departure' : u"10:43",},
        {'name' : u"船町", 'arrival' : u"レ" , 'departure' : u"レ",},
        {'name' : u"下地", 'arrival' : u"レ" , 'departure' : u"レ",},
        {'name' : u"小坂井", 'arrival' : u"10:48" , 'departure' : u"10:48",},
        {'name' : u"牛久保", 'arrival' : u"10:51" , 'departure' : u"10:51",},
        {'name' : u"豊川", 'arrival' : u"10:54" , 'departure' : u"10:55",},
        {'name' : u"三河一宮", 'arrival' : u"10:59" , 'departure' : u"11:00",},
        {'name' : u"長山", 'arrival' : u"11:03" , 'departure' : u"11:06",},
        {'name' : u"江島", 'arrival' : u"11:08" , 'departure' : u"11:09",},
        {'name' : u"東上", 'arrival' : u"11:12" , 'departure' : u"11:12",},
        {'name' : u"野田城", 'arrival' : u"11:15" , 'departure' : u"11:16",},
        {'name' : u"新城", 'arrival' : u"11:19" , 'departure' : u"11:25",},
        {'name' : u"東新町", 'arrival' : u"11:27" , 'departure' : u"11:27",},
        {'name' : u"茶臼山", 'arrival' : u"11:29" , 'departure' : u"11:30",},
        {'name' : u"三河東郷", 'arrival' : u"11:32" , 'departure' : u"11:33",},
        {'name' : u"大海", 'arrival' : u"11:36" , 'departure' : u"11:37",},
        {'name' : u"鳥居", 'arrival' : u"11:39" , 'departure' : u"11:40",},
        {'name' : u"長篠城", 'arrival' : u"11:42" , 'departure' : u"11:43",},
        {'name' : u"本長篠", 'arrival' : u"11:45" , 'departure' : u"11:58",},
        {'name' : u"三河大野", 'arrival' : u"12:03" , 'departure' : u"12:03",},
        {'name' : u"湯谷温泉", 'arrival' : u"12:07" , 'departure' : u"12:07",},
        {'name' : u"三河槙原", 'arrival' : u"12:10" , 'departure' : u"12:14",},
        {'name' : u"柿平", 'arrival' : u"12:17" , 'departure' : u"12:17",},
        {'name' : u"三河川合", 'arrival' : u"12:20" , 'departure' : u"12:21",},
        {'name' : u"池場", 'arrival' : u"12:27" , 'departure' : u"12:28",},
        {'name' : u"東栄", 'arrival' : u"12:29" , 'departure' : u"12:30",},
        {'name' : u"出馬", 'arrival' : u"12:35" , 'departure' : u"12:35",},
        {'name' : u"上市場", 'arrival' : u"12:37" , 'departure' : u"12:37",},
        {'name' : u"浦川", 'arrival' : u"12:40" , 'departure' : u"12:40",},
        {'name' : u"早瀬", 'arrival' : u"12:42" , 'departure' : u"12:42",},
        {'name' : u"下川合", 'arrival' : u"12:44" , 'departure' : u"12:45",},
        {'name' : u"中部天竜", 'arrival' : u"12:48" , 'departure' : u"12:50",},
        {'name' : u"佐久間", 'arrival' : u"12:52" , 'departure' : u"12:52",},
        {'name' : u"相月", 'arrival' : u"12:57" , 'departure' : u"12:58",},
        {'name' : u"城西", 'arrival' : u"13:01" , 'departure' : u"13:01",},
        {'name' : u"向市場", 'arrival' : u"13:05" , 'departure' : u"13:05",},
        {'name' : u"水窪", 'arrival' : u"13:07" , 'departure' : u"13:07",},
        {'name' : u"大嵐", 'arrival' : u"13:14" , 'departure' : u"13:14",},
        {'name' : u"小和田", 'arrival' : u"13:19" , 'departure' : u"13:19",},
        {'name' : u"中井侍", 'arrival' : u"13:24" , 'departure' : u"13:25",},
        {'name' : u"伊那小沢", 'arrival' : u"13:28" , 'departure' : u"13:31",},
        {'name' : u"鶯巣", 'arrival' : u"13:34" , 'departure' : u"13:34",},
        {'name' : u"平岡", 'arrival' : u"13:37" , 'departure' : u"13:38",},
        {'name' : u"為栗", 'arrival' : u"13:44" , 'departure' : u"13:45",},
        {'name' : u"温田", 'arrival' : u"13:50" , 'departure' : u"13:50",},
        {'name' : u"田本", 'arrival' : u"13:53" , 'departure' : u"13:53",},
        {'name' : u"門島", 'arrival' : u"13:58" , 'departure' : u"13:59",},
        {'name' : u"唐笠", 'arrival' : u"14:03" , 'departure' : u"14:04",},
        {'name' : u"金野", 'arrival' : u"14:07" , 'departure' : u"14:08",},
        {'name' : u"千代", 'arrival' : u"14:10" , 'departure' : u"14:10",},
        {'name' : u"天竜峡", 'arrival' : u"14:13" , 'departure' : u"14:15",},
        {'name' : u"川路", 'arrival' : u"14:17" , 'departure' : u"14:17",},
        {'name' : u"時又", 'arrival' : u"14:20" , 'departure' : u"14:20",},
        {'name' : u"駄科", 'arrival' : u"14:23" , 'departure' : u"14:24",},
        {'name' : u"毛賀", 'arrival' : u"14:26" , 'departure' : u"14:27",},
        {'name' : u"伊那八幡", 'arrival' : u"14:28" , 'departure' : u"14:29",},
        {'name' : u"下山村", 'arrival' : u"14:31" , 'departure' : u"14:32",},
        {'name' : u"鼎", 'arrival' : u"14:34" , 'departure' : u"14:34",},
        {'name' : u"切石", 'arrival' : u"14:37" , 'departure' : u"14:38",},
        {'name' : u"飯田", 'arrival' : u"14:41" , 'departure' : u"14:51",},
        {'name' : u"桜町", 'arrival' : u"14:53" , 'departure' : u"14:54",},
        {'name' : u"伊那上郷", 'arrival' : u"14:56" , 'departure' : u"14:56",},
        {'name' : u"元善光寺", 'arrival' : u"15:00" , 'departure' : u"15:01",},
        {'name' : u"下市田", 'arrival' : u"15:03" , 'departure' : u"15:04",},
        {'name' : u"市田", 'arrival' : u"15:06" , 'departure' : u"15:06",},
        {'name' : u"下平", 'arrival' : u"15:10" , 'departure' : u"15:10",},
        {'name' : u"山吹", 'arrival' : u"15:12" , 'departure' : u"15:13",},
        {'name' : u"伊那大島", 'arrival' : u"15:18" , 'departure' : u"15:23",},
        {'name' : u"上片桐", 'arrival' : u"15:29" , 'departure' : u"15:30",},
        {'name' : u"伊那田島", 'arrival' : u"15:32" , 'departure' : u"15:33",},
        {'name' : u"高遠原", 'arrival' : u"15:42" , 'departure' : u"15:42",},
        {'name' : u"七久保", 'arrival' : u"15:45" , 'departure' : u"15:45",},
        {'name' : u"伊那本郷", 'arrival' : u"15:50" , 'departure' : u"15:50",},
        {'name' : u"飯島", 'arrival' : u"15:55" , 'departure' : u"15:56",},
        {'name' : u"田切", 'arrival' : u"15:59" , 'departure' : u"16:00",},
        {'name' : u"伊那福岡", 'arrival' : u"16:05" , 'departure' : u"16:09",},
        {'name' : u"小町屋", 'arrival' : u"16:11" , 'departure' : u"16:12",},
        {'name' : u"駒ヶ根", 'arrival' : u"16:14" , 'departure' : u"16:22",},
        {'name' : u"大田切", 'arrival' : u"16:24" , 'departure' : u"16:25",},
        {'name' : u"宮田", 'arrival' : u"16:27" , 'departure' : u"16:28",},
        {'name' : u"赤木", 'arrival' : u"16:29" , 'departure' : u"16:30",},
        {'name' : u"沢渡", 'arrival' : u"16:34" , 'departure' : u"16:37",},
        {'name' : u"下島", 'arrival' : u"16:39" , 'departure' : u"16:39",},
        {'name' : u"伊那市", 'arrival' : u"16:43" , 'departure' : u"16:44",},
        {'name' : u"伊那北", 'arrival' : u"16:46" , 'departure' : u"16:47",},
        {'name' : u"田畑", 'arrival' : u"16:49" , 'departure' : u"16:50",},
        {'name' : u"北殿", 'arrival' : u"16:53" , 'departure' : u"16:53",},
        {'name' : u"木ノ下", 'arrival' : u"16:56" , 'departure' : u"16:57",},
        {'name' : u"伊那松島", 'arrival' : u"16:59" , 'departure' : u"17:02",},
        {'name' : u"沢", 'arrival' : u"17:05" , 'departure' : u"17:06",},
        {'name' : u"羽場", 'arrival' : u"17:09" , 'departure' : u"17:09",},
        {'name' : u"伊那新町", 'arrival' : u"17:12" , 'departure' : u"17:13",},
        {'name' : u"宮木", 'arrival' : u"17:15" , 'departure' : u"17:15",},
        {'name' : u"辰野", 'arrival' : u"17:18" , 'departure' : u"17:19",},
        {'name' : u"川岸", 'arrival' : u"17:26" , 'departure' : u"17:26",},
        {'name' : u"岡谷", 'arrival' : u"17:30" , 'departure' : u"17:31",},
        {'name' : u"下諏訪", 'arrival' : u"17:35" , 'departure' : u"17:35",},
        {'name' : u"上諏訪", 'arrival' : u"17:40" , 'departure' : None,},
        ]
    }

class TestSequenceFunctions(unittest.TestCase):
    u"""OudiaFormatのテスト"""
    def _add_station(self, attr):
        u"""テストの準備としてDBに路線情報を仕込む"""
        return self.db_obj.addTimetableAttr(attr['company'],
                                            attr['railway_line'],
                                            attr['updown'],
                                            attr['day'],
                                            attr['direction'])

    def _gen_timetable(self, timetable_id):
        u"""テストの準備としてTimetableオブジェクトを返す"""
        return ekikaradb.Timetable(db=self.db_obj,
                                   target_timetable_id=timetable_id,
                                   logger=self.logger)

    def setUp(self) :
        self.logger = gettimetablelog.logging.getLogger("gettimetable")
        self.db_filename = "test_oudiaformat.sqlite"
        self.db_obj = ekikaradb.EkikaraDb(logger=self.logger,
                                          filename=self.db_filename)
        self.timetable_id = self._add_station(ATTR[0])
        target = self._gen_timetable(self.timetable_id)
        target.setLineStation(STATION_LIST_1)
        target.setLineStation(STATION_LIST_2)
        target.setLineStation(STATION_LIST_3)
        target.setTrain(self.timetable_id, TRAIN_1)

    def tearDown(self):
        self.db_obj.close()
        os.unlink(self.db_filename)

    def test_export_format(self):
        u"""oud形式で出力するテスト"""
        fmt = oudiaformat.OudiaFormat(self.db_obj,
                                      self.timetable_id,
                                      self.logger,
                                      sys.argv)
        fout = codecs.open("test_oudiaformat.oud", "wb", fmt.encode)
        fmt.put(fout)
        fout.close()

if __name__ == "__main__" :
    unittest.main()

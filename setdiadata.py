#!/usr/bin/env python
# -*- coding:utf-8 -*-
# えきから時刻表の列車別詳細時刻表からデータを抽出し、OuDia用のファイルを生成
#
# installing BeautifulSoup:
#        http://www.crummy.com/software/BeautifulSoup/
#        check python ver and get 3.0xxx or 3.1xxx, and python setup.py install
u"""gettimetable.pyのスクレイピング結果のsqliteデータよりoudファイルを生成する"""
import codecs
import locale
import sys
from optparse import OptionParser
import ekikaradb
import gettimetablelog
from oudiacsv import OudiaCsv
from oudiaformat import OudiaFormat


def main():
    u""" main routine :-)"""
    console_encode = locale.getpreferredencoding()
    logger = gettimetablelog.logging.getLogger("setdiadatalog")
    # 生の状態のコマンドラインをcommand_lineに保存しておく
    # *.oudファイルのコメントに残しておく為
    command_line = [cmd_item.decode(console_encode) for cmd_item in sys.argv]
    # parse command line
    opt_parser = OptionParser(usage="%prog -d DBFILE -f FILENAME [options] ",
                              version="%prog 0.4")
    opt_parser.add_option("-d", "--db-file",
                          dest="db_file",
                          help="specify sqlite db file.",
                          metavar="FILE")
    opt_parser.add_option("-f", "--output-file",
                          dest="output_file",
                          help=("export filename. less then stdout, "
                                "add ext output format (default .oud)"),
                          metavar="OUTFILE")
    opt_parser.add_option("-o", "--output-format",
                          dest="output_format",
                          default="oud",
                          #help="csv, oud or WinDia",
                          help="csv, oud",
                          metavar="TYPE")
    opt_parser.add_option("-s", "--status",
                          action="store_true",
                          dest="status",
                          help="print status messages.")
    opt_parser.add_option("-t", "--timetable-id",
                          dest="target_timetable_id",
                          default=0,
                          help=("target TIMETABLE-ID. default is 0. "
                                "Listing timetable-id by infotimetable.py -l"),
                          metavar="TIMETABLE-ID")
    opt_parser.add_option("", "--station-list",
                          dest="station_list",
                          help="specify station list FIlE",
                          metavar="FILE")
    opt_parser.add_option("", "--station-list-encode",
                          dest="station_list_encode",
                          default="CP932",
                          help=("file ENCODE for station list. "
                                "default is CP932"),
                          metavar="ENCODE")
    opt_parser.add_option("", "--no-check-order",
                          action="store_false",
                          dest="is_check_order",
                          default=True,
                          help=("no checking train stop time order. "
                                "default checking order"))
    (options, arguments) = opt_parser.parse_args()
    # check cmdline params.
    if options.db_file:
        db_obj = ekikaradb.EkikaraDb(logger=logger,
                                 filename=options.db_file)
    else:
        opt_parser.error(u"missing -d DBFILE and -f OUTFILE")
        logger.info(u"missing -d DBFILE and -f OUTFILE")
        sys.exit(1)
    #--------- main
    target = ekikaradb.Timetable(
        db=db_obj,
        target_timetable_id=options.target_timetable_id,
        logger=logger)
    if options.output_format == "csv" or options.output_format == "oud":
        if options.output_format == "csv":
            # exporter is TimetableExporter object.
            exporter = OudiaCsv(db=db_obj,
                                timetable_id=target.getId(),
                                logger=logger,
                                argv=command_line,
                                status_option=options.status,
                                console_encode=console_encode,
                                )
        elif options.output_format == "oud":
            exporter = OudiaFormat(db=db_obj,
                                   timetable_id=target.getId(),
                                   logger=logger,
                                   argv=command_line,
                                   status_option=options.status,
                                   console_encode=console_encode,
                                   isCheckOrder=options.is_check_order,
                                   )
            if options.station_list:
                exporter.setStationList(
                    options.station_list,
                    options.station_list_encode)
        if options.output_file:
            fout = codecs.open(
                options.output_file + "." + options.output_format,
                "wb",
                exporter.encode)
            exporter.put(fout)
            fout.close()
        else:
            sys.stdout = codecs.getwriter(exporter.encode)(sys.stdout)
            if sys.platform == "win32":
                import msvcrt
                import os
                # stdoutをbinmodeに(U*nix系は元々binmode)
                msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)
            exporter.put(sys.stdout)
        if options.status:
            sys.stderr.write(u"done.\n".encode(console_encode))
    else:
        opt_parser.error(u"unknown output format type=[%s]" %
                        (options.output_format))
        logger.info(u"unknown output format type=[%s]\n" %
                    (options.output_format))
        sys.exit(1)

if __name__ == "__main__":
    main()

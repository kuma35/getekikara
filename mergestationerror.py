#!/usr/bin/env python
# -*- coding:utf-8 -*-


class MergeStationError(Exception):
    def __init__(self, db, list):
        self.db = db
        self.list = list

    def __str__(self):
        return repr(self.list)

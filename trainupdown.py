#!/usr/bin/env python
# -*- coding:utf-8 -*-
u"""上り下りをup,downに変換する。
上り下りだけでなく外回り内回りや東行南行等各種あるのをup,downに落とす
"""

from pprint import PrettyPrinter

NAMES = {
    u"下り": 'down',  # 一般
    u"上り": 'up',  # 一般
    u"外回り": 'down',  # 山手線
    u"内回り": 'up',  # 山手線
    u"左回り": 'down',  # [名古屋市営]名城線 ナゴヤドーム前矢田方面（左回り）
    u"右回り": 'up',  # [名古屋市営]名城線 ナゴヤドーム前矢田方面（右回り）
    u"東行": 'down',  # 都営新宿線
    u"西行": 'up',  # 都営新宿線
    u"南行": 'down',  # [ＪＲ]京浜東北線 横浜方面(南行) 平日・湘南新宿ライン、都営浅草線・三田線
    u"北行": 'up',  # [ＪＲ]京浜東北線 大宮方面(北行) 平日・湘南新宿ライン、都営浅草線・三田線
    u"都庁前": 'down',  # [都営]大江戸線 都庁前方面
    u"光が丘": 'up',  # [都営]大江戸線 光が丘方面
    }


COMPANIES = {
    u"伊予鉄道": {
        # 2011/01/23 by kuma35
        # up,downはえきから時刻表の扱いに合わせた(2系統左回りはえきから上は下り扱い)
        # ４系統は欠番?
        u"１系統(環状線)": {u"右回り": 'down',},     # 右回りのみ
        u"２系統(環状線)": {u"左回り": 'down',},     # 左回りのみ
        u"３系統(市駅線)": {u"道後温泉": 'down', u"松山市駅前": 'up',},
        u"５系統(松山駅前線)": {u"道後温泉": 'down', u"松山駅前": 'up',},
        u"６系統(本町線)": {u"道後温泉": 'down', u"本町６丁目": 'up',},
        },
    u"富山地鉄": {
        u"市内環状線": {u"富山駅前": 'down',}, # 富山駅前方面のみ(片方向のみ)
        u"市内線": {u"大学前": 'down', u"南富山駅前": 'up',},
        u"立山線": {u"立山": 'down', u"寺田": 'up',},
        u"不二越・上滝線": {u"岩峅寺":     'down', u"電鉄富山": 'up',},
        u"本線": {u"宇奈月温泉": 'down', u"電鉄富山": 'up',},
        },
    }

class TrainUpDown(object):
    u""" conver to 'up' or 'down'
    """
    pretty = PrettyPrinter(indent=2)

    def __init__(self, logger):
        u"""initialize
        """
        self.logger = logger       # logging object

    def get_up_down(self, attr):
        u""" return 'down' or 'up' or ''

        """
        self.logger.debug(TrainUpDown.pretty.pformat(attr))
        result = ''
        try:
            # 2011/01/23 by kuma35
            # 伊予鉄の方面の後にはかっこが付かないので、
            # directionをupdownに入れる処理を ParseEkikara で行っている。
            result = COMPANIES[attr['company']][attr['line']][attr['updown']]
        except KeyError:
            try:
                result = NAMES[attr['updown']]
            except KeyError:
                pass
        return result


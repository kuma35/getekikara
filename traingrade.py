#!/usr/bin/env python
# -*- coding: utf-8 -*-
# train grade
# 記入方法:(2011/01/13 by kuma35)
# 1.OUD_BLOCK〜OUD_BLOCK_ENDまでが1つ分の定義です。
# u"Ressyasyubetsu"+OUD_BLOCK,
# u"Syubetsumei=普通",
# u"JikokuhyouMojiColor=00000000",
# u"DiagramSenColor=00000000",
# u"DiagramSenStyle=SenStyle_Jissen",
# u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
# OUD_BLOCKEND,
# 2 ] の直前に加えてください。
# 3.Syubetsumeiの値でえきから時刻表の列車名から判別します。
# 4.時刻表の種別表示が不要の時はRyakusyouの行は不要。
# 5.色替えの必要が無くても列車種別は分けておくと何かと便利です。
# ※このファイルはparseekikara.pyとoudiaformat.pyで利用されます。
#   parseekikara.py:Syubetsumeiより正規表現生成
#   oudiaformat.py:下記の順番を元に列車種別コードを取得します。
u"""列車の等級を認識し、かつ、そのプレフィックスとポストフィックスを取得"""
import re
from oudiaformatdefs import OUD_BLOCK, OUD_BLOCKEND

OUD_TRAIN_GRADE = [u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=普通",
                   u"JikokuhyouMojiColor=00000000",
                   u"DiagramSenColor=00000000",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=快速",
                   u"Ryakusyou=快速",
                   u"JikokuhyouMojiColor=0000C000",
                   u"DiagramSenColor=0000C000",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=急行",
                   u"Ryakusyou=急行",
                   u"JikokuhyouMojiColor=00FFA500",
                   u"DiagramSenColor=00FFA500",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=特急",
                   u"Ryakusyou=特急",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=快特",
                   u"Ryakusyou=快特",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=準急",
                   u"Ryakusyou=準急",
                   u"JikokuhyouMojiColor=0000C000",
                   u"DiagramSenColor=0000C000",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線のぞみ",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線ひかり",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=00FFA500",
                   u"DiagramSenColor=00FFA500",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線こだま",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線つばめ",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線やまびこ",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線つばさ",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線はやて",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線Ｍａｘやまびこ",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線なすの",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線とき",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"JikokuhyouMojiColor=00000000",
                   u"DiagramSenColor=00000000",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線たにがわ",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線Ｍａｘとき",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線あさま",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=ライナー",
                   u"Ryakusyou=快速",
                   u"JikokuhyouMojiColor=00000000",
                   u"DiagramSenColor=00000000",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   u"Ressyasyubetsu" + OUD_BLOCK,
                   u"Syubetsumei=新幹線はやぶさ",
                   u"Ryakusyou=新幹",
                   u"JikokuhyouMojiColor=000000FF",
                   u"DiagramSenColor=000000FF",
                   u"DiagramSenStyle=SenStyle_Jissen",
                   u"StopMarkDrawType=EStopMarkDrawType_DrawOnStop",
                   OUD_BLOCKEND,
                   ]


class TrainGrade(object):
    u"""列車の等級を認識し、かつ、そのプレフィックスとポストフィックスを取得"""
    def __init__(self, logger, grade_list=None):
        self.logger = logger       # logging object
        if grade_list :
            self.grade_list = grade_list
        else:
            self.grade_list = OUD_TRAIN_GRADE
        # name_list 取得
        re_grade_name = re.compile(u"^Syubetsumei=(.*)$", re.U)
        name_list = [re_grade_name.search(line).group(1)
                     for line in self.grade_list if re_grade_name.search(line)]
        self.logger.debug(u"TrainGrade:name_list=[%s]" % (u".".join(name_list)))
        # re_type_obj 生成
        re_type_str = u"^(.*)(" + u"|".join(name_list) + u")(.*)"
        self.logger.debug(u"re_type_str=[%s]" % (re_type_str))
        self.re_type_obj = re.compile(re_type_str, re.U)
        # name 転置マップ生成
        self.name_map = {}
        for index, value in enumerate(name_list):
            self.name_map[value] = index

    def get_code(self, grade_name):
        u"""名前に対応する番号を得る。番号は定義順に決まる"""
        if grade_name in self.name_map:
            return self.name_map[grade_name]
        else:
            return 0

    @property
    def grade_list_formatted(self):
        u"""名前だけの定義順のリスト。"""
        return self.grade_list

    @property
    def re_type(self):
        u"""名前とプレフィックス、ポストフィックスを分離する正規表現を返す"""
        return self.re_type_obj

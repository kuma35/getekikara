#!/usr/bin/env python
# -*- coding: utf-8 -*-

import locale
CONSOLE_ENCODE = locale.getpreferredencoding()

import sys
import trainupdown
import unittest
import gettimetablelog

class TestSequenceFunctions(unittest.TestCase) :
    def setUp(self) :
        self.logger = gettimetablelog.logging.getLogger("gettimetable")
        self.attrs = [
            {'company' : u"ＪＲ",
             'line' : u"只見線",
             'direction' : u"小出",
             'updown' : u"下り",
             'result' : 'down',
             },
            {'company' : u"ＪＲ",
             'line' : u"只見線",
             'direction' : u"会津若松",
             'updown' : u"上り",
             'result' : 'up',
             },
            {'company' : u"ＪＲ",
             'line' : u"山手線",
             'direction' : u"大崎",
             'updown' : u"外回り",
             'result' : 'down',
             },
            {'company' : u"ＪＲ",
             'line' : u"山手線",
             'direction' : u"新宿・池袋",
             'updown' : u"内回り",
             'result' : 'up',
             },
            {'company' : u"名古屋市営", 
             'line' : u"名城線",
             'direction' : u"ナゴヤドーム前矢田",
             'updown' : u"左回り",
             'result' : 'down',
             },
            {'company' : u"名古屋市営", 
             'line' : u"名城線",
             'direction' : u"ナゴヤドーム前矢田",
             'updown' : u"右回り",
             'result' : 'up',
             },
            {'company' : u"都営", 
             'line' : u"新宿線",
             'direction' : u"本八幡",
             'updown' : u"東行",
             'result' : 'down',
             },
            {'company' : u"都営", 
             'line' : u"新宿線",
             'direction' : u"新宿",
             'updown' : u"西行",
             'result' : 'up',
             },
            {'company' : u"ＪＲ", 
             'line' : u"京浜東北線",
             'direction' : u"",
             'updown' : u"南行",
             'result' : 'down',
             },
            {'company' : u"ＪＲ", 
             'line' : u"京浜東北線",
             'direction' : u"",
             'updown' : u"北行",
             'result' : 'up',
             },
            {'company' : u"伊予鉄道", 
             'line' : u"１系統(環状線)",
             'direction' : u"松山市駅前",
             'updown' : u"右回り",
             'result' : 'down',
             },
            {'company' : u"伊予鉄道", 
             'line' : u"２系統(環状線)",
             'direction' : u"松山市駅前",
             'updown' : u"左回り",
             'result' : 'down',
             },
            {'company' : u"伊予鉄道", 
             'line' : u"３系統(市駅線)",
             'direction' : u"道後温泉",
             'updown' : u"道後温泉",
             'result' : 'down',
             },
            {'company' : u"伊予鉄道", 
             'line' : u"３系統(市駅線)",
             'direction' : u"松山市駅前",
             'updown' : u"松山市駅前",
             'result' : 'up',
             },
            {'company' : u"伊予鉄道", 
             'line' : u"５系統(松山駅前線)",
             'direction' : u"道後温泉",
             'updown' : u"道後温泉",
             'result' : 'down',
             },
            {'company' : u"伊予鉄道", 
             'line' : u"５系統(松山駅前線)",
             'direction' : u"松山駅前",
             'updown' : u"松山駅前",
             'result' : 'up',
             },
            {'company' : u"伊予鉄道", 
             'line' : u"６系統(本町線)",
             'direction' : u"道後温泉",
             'updown' : u"道後温泉",
             'result' : 'down',
             },
            {'company' : u"伊予鉄道", 
             'line' : u"６系統(本町線)",
             'direction' : u"本町６丁目",
             'updown' : u"本町６丁目",
             'result' : 'up',
             },
            {'company' : u"都営", 
             'line' : u"上野懸垂線",    #えきから時刻表には未掲載(2011/01/24現在)
             'direction' : u"上野動物園西園",
             'updown' : u"上野動物園西園",
             'result' : '',
             },
            {'company' : u"都営", 
             'line' : u"上野懸垂線",    #えきから時刻表には未掲載(2011/01/24現在)
             'direction' : u"上野動物園東園",
             'updown' : u"上野動物園東園",
             'result' : '',
             },
            ]

    def tearDown(self) :
        pass

    def testPutList(self) :
        updown = trainupdown.TrainUpDown(self.logger)
        for attr in self.attrs :
            result = updown.get_up_down(attr)
            sys.stderr.write((u"[%s]%s %s方面(%s) expected[%s]result[%s]\n"%(attr['company'],
                                                                             attr['line'],
                                                                             attr['direction'],
                                                                             attr['updown'],
                                                                             attr['result'],
                                                                             result)).encode(CONSOLE_ENCODE))
            self.assertEqual(attr['result'], result, "invalid result!!")


if __name__ == "__main__" :
    unittest.main()

#!/usr/bin/env python
# -*- coding:utf-8 -*-
# えきから時刻表の列車別詳細時刻表からデータを抽出し、sliteDBファイルに保存
#
# installing BeautifulSoup:
#        http://www.crummy.com/software/BeautifulSoup/
#        check python ver and get 3.0xxx or 3.1xxx, and python setup.py install
u"""えきから時刻表より指定の表形式時刻表の路線の全列車詳細を取得(上下は別々)

えきから時刻表の路線(上下は別々に取得)の全列車詳細を取得して
指定のsqliteファイルへ書き出す。
"""
import locale
from optparse import OptionParser
import sys
import urllib2
from ekikaradb import EkikaraDb, Timetable
import gettimetablelog
from parseekikara import ParseEkikara
from pageopener import PageOpener, PageOpenerRetryError
import trainupdown


def main():
    u"""main routine :-)"""
    console_encode = locale.getpreferredencoding()
    logger = gettimetablelog.logging.getLogger("gettimetable")
    # parse command line
    opt_parser = OptionParser(usage=("%prog -d DBFILE "
                                     "[options] [URL [ URL... ]]"),
                              version="%prog 0.4")
    opt_parser.add_option("-p", "--proxy",
                          dest="proxy",
                          help=("proxy if you need."
                                "(OK: hogehoge.com:8080 ,"
                                "NG: http://hogehoge.com:8080)"),
                          metavar="URL")
    opt_parser.add_option("-s", "--status",
                          action="store_true",
                          dest="status",
                          help="print status messages.")
    opt_parser.add_option("-d", "--db-file",
                          dest="db_file",
                          default="out.sqlite",
                          help=("specify sqlite db file."
                                "If nothing then make,"
                                " if exist then update this."),
                          metavar="DBFILE")
    opt_parser.add_option("-n", "--no-separate-train",
                          dest="sep_train",
                          action="store_false",
                          default=True,
                          help=("If having multiple train numbers, "
                                "separate train timetable by train number."))
    # timeout and retry options are extend connection timeout. for subway :-)
    opt_parser.add_option("", "--timeout",
                          action="store",
                          type="float",
                          dest="timeout",
                          help="set timeout. max wait is SEC x COUNT is ",
                          metavar="SEC")
    opt_parser.add_option("", "--retry",
                          action="store",
                          type="int",
                          dest="retry",
                          help="set retry count max. max wait is SEC x COUNT",
                          metavar="COUNT")
    (options, arguments) = opt_parser.parse_args()
    db_obj = EkikaraDb(logger=logger, filename=options.db_file)
    #--------- main
    if options.timeout and options.retry:
        page_opender = PageOpener(logger,
                                proxy=options.proxy,
                                timeout=options.timeout,
                                retryMax=options.retry)
    else:
        page_opender = PageOpener(logger,
                                proxy=options.proxy)
    parser = ParseEkikara(logger=logger,
                          status_option=options.status,
                          console_encode=console_encode,
                          is_sep_train=options.sep_train)
    up_down_obj = trainupdown.TrainUpDown(logger)
    try:
        for url in arguments:
            logger.info(u"start parsing: %s" % (url))
            if options.status:
                sys.stderr.write((u"\nstart parsing: %s\n" %
                                  (url)).encode(console_encode))
            attr = parser.getTimeTableAttr(page_opender, url)
            updown = up_down_obj.get_up_down(attr)
            if updown == "":
                updown = 'down'  # 不明な場合はdown扱い
            target = Timetable(
                db=db_obj,
                target_timetable_id=db_obj.addTimetableAttr(
                    company=attr['company'],
                    railway_line=attr['line'],
                    updown=updown,
                    day=attr['day'],
                    direction=attr['direction']),
                logger=logger)
            station_list = parser.getStationList(page_opender, url, updown)
            target.setLineStation(station_list)
            train_order = target.getMaxTrainOrder() + 1
            for detail_url in parser.getDetails(page_opender, url):
                if not target.isExistTrainUrl(detail_url):
                    train = parser.getTrain(page_opender, detail_url)
                    for single_train in parser.splitTrain(train):
                        if not target.isExistSameTrain(single_train,
                                                       detail_url):
                            single_train.update(
                                parser.extractTrainName(single_train['name']))
                            train_order += 1
                            target.setTrain(train_order, single_train)
                            msg_single_train_to_db = (
                                u"wroteDb[%s][%s][%s][%s][%s][%s]" %
                                (single_train['number'],
                                 single_train['type_prefix'],
                                 single_train['type'],
                                 single_train['name'],
                                 single_train['gou'],
                                 single_train['url'],))
                            logger.debug(msg_single_train_to_db)
                            if options.status:
                                sys.stderr.write(
                                    (msg_single_train_to_db
                                     ).encode(console_encode))
            logger.info(u"parsing end: %s" % (url))
            if options.status:
                sys.stderr.write((u"\nparsing end: %s\n" %
                                  (url)).encode(console_encode))
    except PageOpenerRetryError, err_obj:
        #urllib2.URLError: <urlopen error [Errno 11001] getaddrinfo failed>
        logger.error(u"Over retry count;retryMax=[%d], retryCount=[%d]\n" %
                     (err_obj.retryMax, err_obj.retryCount))
        sys.stderr.write(((u"Over retry count;retryMax=[%d], "
                           u"retryCount=[%d]\n") %
                          (err_obj.retryMax,
                           err_obj.retryCount)).encode(console_encode))
        sys.exit(1)  # 終了するよ
    except urllib2.HTTPError, err_obj:
        logger.error(u"invalid URL[%s][%d].\nPlease check url.\n" %
                     (url, err_obj.code))
        sys.stderr.write((u"invalid URL[%s][%d].\nPlease check url.\n" %
                          (url, err_obj.code)).encode(console_encode))
        sys.exit(1)  # 終了するよ
    except urllib2.URLError, err_obj:
        #urllib2.URLError: <urlopen error [Errno 11001] getaddrinfo failed>
        logger.error(u"Can not open URL[%s].\n"
                     u"%s\nPlease check your internet access.\n" %
                     (url, err_obj.reason))
        sys.stderr.write((u"Can not open URL[%s].\n"
                          u"%s\nPlease check your internet access.\n" %
                          (url, err_obj.reason)).encode(console_encode))
        sys.exit(1)  # 終了するよ

if __name__ == "__main__":
    main()

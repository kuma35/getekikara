#!/usr/bin/env python
# -*- coding:utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding('utf-8')  # for BeautifulSoup
import re
import locale
import BeautifulSoup
import pageopener
import parseekikara

RE_TITLE_TRAIN_NUMBER = re.compile(u"列車番号")
RE_TITLE_TRAIN_NAME = re.compile(u"列車名")
RE_TITLE_NOTICE = re.compile(u"運転日注意")
RE_BRACKET = re.compile(u"^\[(.*)\]$")


class ParseTable(parseekikara.ParseEkikara):
    u"""表の方の時刻表から取得する

    依然として列車時刻の取得も行うが、setdiadataでのマッチングが高速になる。
    列車時刻表とのマッチングを省略すれば更に高速になる。
    オリジナルの組み合わせの路線では一工夫必要。
    """
    def __init__(self,
                 logger,
                 status_option,
                 console_encode=locale.getpreferredencoding(),
                 is_sep_train=True):
        parseekikara.ParseEkikara.__init__(self,
                                           logger,
                                           status_option,
                                           console_encode,
                                           is_sep_train)

    @classmethod
    def _expect(self, soup, reObj):
        u"""指定のタイトル文字列を持つ行(tr)を返す。無ければNoneを返す
        """
        result = soup.find(tag=lambda tag: tag.name == u'span', text=reObj)
        result = result.parent.parent.parent.parent if result is not None else None
        return result

    @classmethod
    def _getHeaderColumns(self, soup):
        colspans = soup.findAll(lambda tag: tag.name == u'td' and not tag.has_key(u'colspan'))
        if colspans is not None:
            for column in colspans:
                value = column.find(lambda tag: tag.name == u'span' and tag.has_key(u'style'))
                value = value.string.strip() if value and value.string else value
                # assert value is not None and value != '', "miss column value!!"
                yield value
        else:
            yield None

    @classmethod
    def _getNameColumns(self, soup):
        u"""えきから時刻表の表形式のページより列車名を取得する

        return item = {'type': u'', 'type_prefix': u'', 'name': u'', 'gou': u''}
        """
        for column in soup.findAll(lambda tag: tag.name == u'td' and not tag.has_key(u'colspan')):
            item = {'type': u'',
                    'type_prefix': u'',
                    'name': u'',
                    'gou': u''}
            item['type'] = column.find(lambda tag:
                                           tag.name == 'span' and tag.has_key('class') and tag['class'] == 's').string
            item['type'] = item['type'].strip() if item['type'] else None
            mBracket = RE_BRACKET.search(item['type'])
            item['type'] = mBracket.group(1) if mBracket else None
            mType = self.grade.reType.search(item['type'])
            ( item['type_prefix'], item['type'] ) = ( mType.group(1), mType.group(2) ) if mType else None
            nameCells = column.find(lambda tag:
                                        tag.name == 'span' and tag.has_key('class') and tag['class'] == 'm')
            for cell in nameCells.contents:
                if cell and cell.string:
                    piece = cell.string.strip()
                    piece = u'' if piece == u'&nbsp;' else piece
                    item['name'] += piece
            mGou = parseekikara.reGou.search(item['name'])
            ( item['gou'], item['name'] ) = ( mGou.group(2), mGou.group(1) ) if mGou else None
            item['name'] = u'' if item['name'] == u'&nbsp' else item['name']
            yield item

    @classmethod
    def _getNoticeColumns(self, soup):
        u"""todo:_getNameColumnsど同様に頑張って追う？
        """
        colspans = soup.findAll(lambda tag: tag.name == u'td' and not tag.has_key(u'colspan'))
        if colspans is not None:
            for column in colspans:
                value = column.find(lambda tag:
                                        tag.name == u'span' and tag.has_key(u'class') and tag['class'] == 'textBold')
                if value and value.string:
                    value = value.string.strip()
                    value = '' if value == '&nbsp;' else value
                yield value
        else:
            yield None

    def _get_station_time_matrix(self, soup):
        u"""横軸:列車番号、縦軸:駅名、値:時間
        
        """
        pass

    def getTableTrains(self, pageOpener, url):
        u"""えきから時刻表の表形式のページをURLで指定して列車情報を取得する
        """
        trains = []     # 配列。parseTableの場合は順番が大きな意味を持つ
        html = pageOpener.open(fullurl=url)
        soup = BeautifulSoup.BeautifulSoup(html)
        # 列車番号を取得
        numberRow = self._expect(soup, RE_TITLE_TRAIN_NUMBER)
        if numberRow is None:
            return trains   # 列車番号が無いとどうにもならないので終了。
        trains = [ {u'number': trainNumber} for trainNumber in self._getHeaderColumns(numberRow) ]
        tbody = numberRow.parent   # 検索範囲を限定する
        del soup
        nameRow = self._expect(tbody, RE_TITLE_TRAIN_NAME)  # 列車名を取得
        if nameRow is not None:
            for i, name in enumerate(self._getNameColumns(nameRow)):  # _getNameColumns() returns dict.
                trains[i].update(name)
        noticeRow = self._expect(tbody, RE_TITLE_NOTICE)  # 運転日注意
        if noticeRow is not None:
            for i, notice in enumerate(self._getNoticeColumns(noticeRow)):
                trains[i]['notice'] = notice
        linkRow = noticeRow.nextSibling  # 詳細ページへのリンクの行
        if linkRow is not None:
            for i, link in enumerate(self._getHeaderColumns(linkRow)):
                trains[i]['url'] = link
        # ダミー行?        dummyRow = linkRow.nextSibling
        # 詳細 路線が分岐する場合は複数のtrに分かれている。駅はbrで区切られる。着発駅はダッシュによる省略あり。
        #      そして「接続」用のtrが必ずあり、それでtbodyが終了となる。
        detailRow = dummyRow.nextSibling
        # stationList
        station_list = self._get_station_list(detailRow)
        time_matrix = self._get_station_time_matrix(detailRow, station_list)

        
# --------------- test
if __name__ == '__main__':
    import gettimetablelog
    from optparse import OptionParser
    opt_parser = OptionParser(usage="%prog [options] URL [ URL... ]", version="%prog 0.2")
    opt_parser.add_option("-p",
                          "--proxy",
                          dest="proxy",
                          help="specify proxy isf you need. less scheme name(NG; http://hogehoge.com:8080, GOOD; hogehoge.com:8080)",
                          metavar="URL")
    #parser.add_option("-d", "--debug", action="store_true", dest="debug", help="a lot of messages for debug")
    opt_parser.add_option("-s", "--status", action="store_true", dest="status", help="status messages to stderr")
    (options, args) = opt_parser.parse_args()
    logger = gettimetablelog.logging.getLogger("gettimetable")
    console_encode = locale.getpreferredencoding()
    page_opener = pageopener.PageOpener(logger=logger,
                                        proxy=options.proxy,
                                        console_encode=console_encode)
    parser = ParseTable(logger=logger,
                        status_option=options.status,
                        console_encode=console_encode)
    train_order = 0
    for url in args:
        stationList = parser.getStationList(pageOpener, url)
        for pageUrl in parser.getPages(page_opener, url):
            for train in parser.getTableTrains(page_opener, pageUrl):
                train.update(parser.extractTrainName(train['name']))
                train_order += 1

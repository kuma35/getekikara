#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ekikaradb
import unittest
import gettimetablelog
import tempfile
import os

import pprint
PP = pprint.PrettyPrinter(indent=4)


class TestSequenceFunctions(unittest.TestCase) :
    def setUp(self) :
        self.logger = gettimetablelog.logging.getLogger("gettimetable")
        self.dbFilename = "testekikaradb.sqlite"
        self.db = ekikaradb.EkikaraDb(logger=self.logger, filename=self.dbFilename)
        self.attr= [{'company' : u"ＪＲ",
                     'railway_line' : u"飯田線",
                     'updown' : u"down",
                     'day' : u"平日",
                     'direction' : u"天竜峡方面",
                     },
                    {'company' : u"ＪＲ",
                     'railway_line' : u"飯田線",
                     'updown' : u"up",
                     'day' : u"平日",
                     'direction' : u"豊橋方面",
                     },]
        self.stationList1 = [
            {'name' : u"豊橋", 'arrival' : False, 'departure' : True},
            {'name' : u"船町", 'arrival' : False, 'departure' : True},
            {'name' : u"下地", 'arrival' : False, 'departure' : True},
            {'name' : u"小坂井", 'arrival' : False, 'departure' : True},
            {'name' : u"牛久保", 'arrival' : False, 'departure' : True},
            {'name' : u"豊川", 'arrival' : True, 'departure' : True},
            ]
        self.stationReverseList1 = [
            {'name' : u"豊川", 'arrival' : False, 'departure' : True},
            {'name' : u"牛久保", 'arrival' : False, 'departure' : True},
            {'name' : u"小坂井", 'arrival' : False, 'departure' : True},
            {'name' : u"下地", 'arrival' : False, 'departure' : True},
            {'name' : u"船町", 'arrival' : False, 'departure' : True},
            {'name' : u"豊橋", 'arrival' : True, 'departure' : False},
            ]
        self.stationList2 = [
            {'name' : u"豊橋", 'arrival' : False, 'departure' : True},
            {'name' : u"船町", 'arrival' : False, 'departure' : True},
            {'name' : u"下地", 'arrival' : False, 'departure' : True},
            {'name' : u"小坂井", 'arrival' : False, 'departure' : True},
            {'name' : u"牛久保", 'arrival' : False, 'departure' : True},
            {'name' : u"豊川", 'arrival' : True, 'departure' : True},
            {'name' : u"三河一宮", 'arrival' : False, 'departure' :True },
            {'name' : u"長山", 'arrival' : True, 'departure' :True },
            {'name' : u"江島", 'arrival' : False, 'departure' :True },
            {'name' : u"東上", 'arrival' : False, 'departure' :True },
            {'name' : u"野田城", 'arrival' : False, 'departure' :True },
            {'name' : u"新城", 'arrival' : True, 'departure' :True },
            {'name' : u"東新町", 'arrival' : False, 'departure' :True },
            {'name' : u"茶臼山", 'arrival' : False, 'departure' :True },
            {'name' : u"三河東郷", 'arrival' : False, 'departure' :True },
            {'name' : u"大海", 'arrival' : False, 'departure' :True },
            {'name' : u"鳥居", 'arrival' : False, 'departure' :True },
            {'name' : u"長篠城", 'arrival' : False, 'departure' :True },
            {'name' : u"本長篠", 'arrival' : True, 'departure' :True },
            {'name' : u"三河大野", 'arrival' : False, 'departure' :True },
            {'name' : u"湯谷温泉", 'arrival' : False, 'departure' :True },
            {'name' : u"三河槙原", 'arrival' : False, 'departure' :True },
            {'name' : u"柿平", 'arrival' : False, 'departure' :True },
            {'name' : u"三河川合", 'arrival' : False, 'departure' :True },
            {'name' : u"池場", 'arrival' : False, 'departure' :True },
            {'name' : u"東栄", 'arrival' : False, 'departure' :True },
            {'name' : u"出馬", 'arrival' : False, 'departure' :True },
            {'name' : u"上市場", 'arrival' : False, 'departure' :True },
            {'name' : u"浦川", 'arrival' : False, 'departure' :True },
            {'name' : u"早瀬", 'arrival' : False, 'departure' :True },
            {'name' : u"下川合", 'arrival' : False, 'departure' :True },
            {'name' : u"中部天竜", 'arrival' : True, 'departure' :True },
            {'name' : u"佐久間", 'arrival' : False, 'departure' :True },
            {'name' : u"相月", 'arrival' : False, 'departure' :True },
            {'name' : u"城西", 'arrival' : False, 'departure' :True },
            {'name' : u"向市場", 'arrival' : False, 'departure' :True },
            {'name' : u"水窪", 'arrival' : True, 'departure' :True },
            {'name' : u"大嵐", 'arrival' : False, 'departure' :True },
            {'name' : u"小和田", 'arrival' : False, 'departure' :True },
            {'name' : u"中井侍", 'arrival' : False, 'departure' :True },
            {'name' : u"伊那小沢", 'arrival' : False, 'departure' :True },
            {'name' : u"鶯巣", 'arrival' : False, 'departure' :True },
            {'name' : u"平岡", 'arrival' : True, 'departure' :True },
            {'name' : u"為栗", 'arrival' : False, 'departure' :True },
            {'name' : u"温田", 'arrival' : False, 'departure' :True },
            {'name' : u"田本", 'arrival' : False, 'departure' :True },
            {'name' : u"門島", 'arrival' : False, 'departure' :True },
            {'name' : u"唐笠", 'arrival' : False, 'departure' :True },
            {'name' : u"金野", 'arrival' : False, 'departure' :True },
            {'name' : u"千代", 'arrival' : False, 'departure' :True },
            {'name' : u"天竜峡", 'arrival' : True, 'departure' :False },
            ]
        self.stationList3 = [
            {'name' : u"天竜峡", 'arrival' : False, 'departure' :True },
            {'name' : u"川路", 'arrival' : False, 'departure' :True },
            {'name' : u"時又", 'arrival' : False, 'departure' :True },
            {'name' : u"駄科", 'arrival' : False, 'departure' :True },
            {'name' : u"毛賀", 'arrival' : False, 'departure' :True },
            {'name' : u"伊那八幡", 'arrival' : False, 'departure' :True },
            {'name' : u"下山村", 'arrival' : False, 'departure' :True },
            {'name' : u"鼎", 'arrival' : False, 'departure' :True },
            {'name' : u"切石", 'arrival' : False, 'departure' :True },
            {'name' : u"飯田", 'arrival' : True, 'departure' :True },
            {'name' : u"桜町", 'arrival' : False, 'departure' :True },
            {'name' : u"伊那上郷", 'arrival' : False, 'departure' :True },
            {'name' : u"元善光寺", 'arrival' : False, 'departure' :True },
            {'name' : u"下市田", 'arrival' : False, 'departure' :True },
            {'name' : u"市田", 'arrival' : False, 'departure' :True },
            {'name' : u"下平", 'arrival' : False, 'departure' :True },
            {'name' : u"山吹", 'arrival' : False, 'departure' :True },
            {'name' : u"伊那大島", 'arrival' : True, 'departure' :True },
            {'name' : u"上片桐", 'arrival' : False, 'departure' :True },
            {'name' : u"伊那田島", 'arrival' : False, 'departure' :True },
            {'name' : u"高遠原", 'arrival' : False, 'departure' :True },
            {'name' : u"七久保", 'arrival' : False, 'departure' :True },
            {'name' : u"伊那本郷", 'arrival' : False, 'departure' :True },
            {'name' : u"飯島", 'arrival' : False, 'departure' :True },
            {'name' : u"田切", 'arrival' : False, 'departure' :True },
            {'name' : u"伊那福岡", 'arrival' : False, 'departure' :True },
            {'name' : u"小町屋", 'arrival' : False, 'departure' :True },
            {'name' : u"駒ヶ根", 'arrival' : True, 'departure' :True },
            {'name' : u"大田切", 'arrival' : False, 'departure' :True },
            {'name' : u"宮田", 'arrival' : False, 'departure' :True },
            {'name' : u"赤木", 'arrival' : False, 'departure' :True },
            {'name' : u"沢渡", 'arrival' : False, 'departure' :True },
            {'name' : u"下島", 'arrival' : False, 'departure' :True },
            {'name' : u"伊那市", 'arrival' : False, 'departure' :True },
            {'name' : u"伊那北", 'arrival' : False, 'departure' :True },
            {'name' : u"田畑", 'arrival' : False, 'departure' :True },
            {'name' : u"北殿", 'arrival' : False, 'departure' :True },
            {'name' : u"木ノ下", 'arrival' : False, 'departure' :True },
            {'name' : u"伊那松島", 'arrival' : True, 'departure' :True },
            {'name' : u"沢", 'arrival' : False, 'departure' :True },
            {'name' : u"羽場", 'arrival' : False, 'departure' :True },
            {'name' : u"伊那新町", 'arrival' : False, 'departure' :True },
            {'name' : u"宮木", 'arrival' : False, 'departure' :True },
            {'name' : u"辰野", 'arrival' : True, 'departure' :False },
            ]
        self.train1 = {
            'name' : None,
            'number' : u"519M",
            'train_order': 0,
            'type' : u"普通",
            'note' : u"[駅着発時刻変更案内]９月２０日　岡谷〜上諏訪　時刻･番線変更あり（以下は通常運転の場合） ,                                                                   ９月４日　岡谷〜上諏訪　時刻･番線変更あり（以下は通常運転の場合）",
            'gou' : None,
            'url' : u"http://www.ekikara.jp/newdata/detail/2301071/62965.htm",
            'stations' : [
                {'name' : u"豊橋", 'arrival' : None , 'departure' : u"10:43",},
                {'name' : u"船町", 'arrival' : u"レ" , 'departure' : u"レ",},
                {'name' : u"下地", 'arrival' : u"レ" , 'departure' : u"レ",},
                {'name' : u"小坂井", 'arrival' : u"10:48" , 'departure' : u"10:48",},
                {'name' : u"牛久保", 'arrival' : u"10:51" , 'departure' : u"10:51",},
                {'name' : u"豊川", 'arrival' : u"10:54" , 'departure' : u"10:55",},
                {'name' : u"三河一宮", 'arrival' : u"10:59" , 'departure' : u"11:00",},
                {'name' : u"長山", 'arrival' : u"11:03" , 'departure' : u"11:06",},
                {'name' : u"江島", 'arrival' : u"11:08" , 'departure' : u"11:09",},
                {'name' : u"東上", 'arrival' : u"11:12" , 'departure' : u"11:12",},
                {'name' : u"野田城", 'arrival' : u"11:15" , 'departure' : u"11:16",},
                {'name' : u"新城", 'arrival' : u"11:19" , 'departure' : u"11:25",},
                {'name' : u"東新町", 'arrival' : u"11:27" , 'departure' : u"11:27",},
                {'name' : u"茶臼山", 'arrival' : u"11:29" , 'departure' : u"11:30",},
                {'name' : u"三河東郷", 'arrival' : u"11:32" , 'departure' : u"11:33",},
                {'name' : u"大海", 'arrival' : u"11:36" , 'departure' : u"11:37",},
                {'name' : u"鳥居", 'arrival' : u"11:39" , 'departure' : u"11:40",},
                {'name' : u"長篠城", 'arrival' : u"11:42" , 'departure' : u"11:43",},
                {'name' : u"本長篠", 'arrival' : u"11:45" , 'departure' : u"11:58",},
                {'name' : u"三河大野", 'arrival' : u"12:03" , 'departure' : u"12:03",},
                {'name' : u"湯谷温泉", 'arrival' : u"12:07" , 'departure' : u"12:07",},
                {'name' : u"三河槙原", 'arrival' : u"12:10" , 'departure' : u"12:14",},
                {'name' : u"柿平", 'arrival' : u"12:17" , 'departure' : u"12:17",},
                {'name' : u"三河川合", 'arrival' : u"12:20" , 'departure' : u"12:21",},
                {'name' : u"池場", 'arrival' : u"12:27" , 'departure' : u"12:28",},
                {'name' : u"東栄", 'arrival' : u"12:29" , 'departure' : u"12:30",},
                {'name' : u"出馬", 'arrival' : u"12:35" , 'departure' : u"12:35",},
                {'name' : u"上市場", 'arrival' : u"12:37" , 'departure' : u"12:37",},
                {'name' : u"浦川", 'arrival' : u"12:40" , 'departure' : u"12:40",},
                {'name' : u"早瀬", 'arrival' : u"12:42" , 'departure' : u"12:42",},
                {'name' : u"下川合", 'arrival' : u"12:44" , 'departure' : u"12:45",},
                {'name' : u"中部天竜", 'arrival' : u"12:48" , 'departure' : u"12:50",},
                {'name' : u"佐久間", 'arrival' : u"12:52" , 'departure' : u"12:52",},
                {'name' : u"相月", 'arrival' : u"12:57" , 'departure' : u"12:58",},
                {'name' : u"城西", 'arrival' : u"13:01" , 'departure' : u"13:01",},
                {'name' : u"向市場", 'arrival' : u"13:05" , 'departure' : u"13:05",},
                {'name' : u"水窪", 'arrival' : u"13:07" , 'departure' : u"13:07",},
                {'name' : u"大嵐", 'arrival' : u"13:14" , 'departure' : u"13:14",},
                {'name' : u"小和田", 'arrival' : u"13:19" , 'departure' : u"13:19",},
                {'name' : u"中井侍", 'arrival' : u"13:24" , 'departure' : u"13:25",},
                {'name' : u"伊那小沢", 'arrival' : u"13:28" , 'departure' : u"13:31",},
                {'name' : u"鶯巣", 'arrival' : u"13:34" , 'departure' : u"13:34",},
                {'name' : u"平岡", 'arrival' : u"13:37" , 'departure' : u"13:38",},
                {'name' : u"為栗", 'arrival' : u"13:44" , 'departure' : u"13:45",},
                {'name' : u"温田", 'arrival' : u"13:50" , 'departure' : u"13:50",},
                {'name' : u"田本", 'arrival' : u"13:53" , 'departure' : u"13:53",},
                {'name' : u"門島", 'arrival' : u"13:58" , 'departure' : u"13:59",},
                {'name' : u"唐笠", 'arrival' : u"14:03" , 'departure' : u"14:04",},
                {'name' : u"金野", 'arrival' : u"14:07" , 'departure' : u"14:08",},
                {'name' : u"千代", 'arrival' : u"14:10" , 'departure' : u"14:10",},
                {'name' : u"天竜峡", 'arrival' : u"14:13" , 'departure' : u"14:15",},
                {'name' : u"川路", 'arrival' : u"14:17" , 'departure' : u"14:17",},
                {'name' : u"時又", 'arrival' : u"14:20" , 'departure' : u"14:20",},
                {'name' : u"駄科", 'arrival' : u"14:23" , 'departure' : u"14:24",},
                {'name' : u"毛賀", 'arrival' : u"14:26" , 'departure' : u"14:27",},
                {'name' : u"伊那八幡", 'arrival' : u"14:28" , 'departure' : u"14:29",},
                {'name' : u"下山村", 'arrival' : u"14:31" , 'departure' : u"14:32",},
                {'name' : u"鼎", 'arrival' : u"14:34" , 'departure' : u"14:34",},
                {'name' : u"切石", 'arrival' : u"14:37" , 'departure' : u"14:38",},
                {'name' : u"飯田", 'arrival' : u"14:41" , 'departure' : u"14:51",},
                {'name' : u"桜町", 'arrival' : u"14:53" , 'departure' : u"14:54",},
                {'name' : u"伊那上郷", 'arrival' : u"14:56" , 'departure' : u"14:56",},
                {'name' : u"元善光寺", 'arrival' : u"15:00" , 'departure' : u"15:01",},
                {'name' : u"下市田", 'arrival' : u"15:03" , 'departure' : u"15:04",},
                {'name' : u"市田", 'arrival' : u"15:06" , 'departure' : u"15:06",},
                {'name' : u"下平", 'arrival' : u"15:10" , 'departure' : u"15:10",},
                {'name' : u"山吹", 'arrival' : u"15:12" , 'departure' : u"15:13",},
                {'name' : u"伊那大島", 'arrival' : u"15:18" , 'departure' : u"15:23",},
                {'name' : u"上片桐", 'arrival' : u"15:29" , 'departure' : u"15:30",},
                {'name' : u"伊那田島", 'arrival' : u"15:32" , 'departure' : u"15:33",},
                {'name' : u"高遠原", 'arrival' : u"15:42" , 'departure' : u"15:42",},
                {'name' : u"七久保", 'arrival' : u"15:45" , 'departure' : u"15:45",},
                {'name' : u"伊那本郷", 'arrival' : u"15:50" , 'departure' : u"15:50",},
                {'name' : u"飯島", 'arrival' : u"15:55" , 'departure' : u"15:56",},
                {'name' : u"田切", 'arrival' : u"15:59" , 'departure' : u"16:00",},
                {'name' : u"伊那福岡", 'arrival' : u"16:05" , 'departure' : u"16:09",},
                {'name' : u"小町屋", 'arrival' : u"16:11" , 'departure' : u"16:12",},
                {'name' : u"駒ヶ根", 'arrival' : u"16:14" , 'departure' : u"16:22",},
                {'name' : u"大田切", 'arrival' : u"16:24" , 'departure' : u"16:25",},
                {'name' : u"宮田", 'arrival' : u"16:27" , 'departure' : u"16:28",},
                {'name' : u"赤木", 'arrival' : u"16:29" , 'departure' : u"16:30",},
                {'name' : u"沢渡", 'arrival' : u"16:34" , 'departure' : u"16:37",},
                {'name' : u"下島", 'arrival' : u"16:39" , 'departure' : u"16:39",},
                {'name' : u"伊那市", 'arrival' : u"16:43" , 'departure' : u"16:44",},
                {'name' : u"伊那北", 'arrival' : u"16:46" , 'departure' : u"16:47",},
                {'name' : u"田畑", 'arrival' : u"16:49" , 'departure' : u"16:50",},
                {'name' : u"北殿", 'arrival' : u"16:53" , 'departure' : u"16:53",},
                {'name' : u"木ノ下", 'arrival' : u"16:56" , 'departure' : u"16:57",},
                {'name' : u"伊那松島", 'arrival' : u"16:59" , 'departure' : u"17:02",},
                {'name' : u"沢", 'arrival' : u"17:05" , 'departure' : u"17:06",},
                {'name' : u"羽場", 'arrival' : u"17:09" , 'departure' : u"17:09",},
                {'name' : u"伊那新町", 'arrival' : u"17:12" , 'departure' : u"17:13",},
                {'name' : u"宮木", 'arrival' : u"17:15" , 'departure' : u"17:15",},
                {'name' : u"辰野", 'arrival' : u"17:18" , 'departure' : u"17:19",},
                {'name' : u"川岸", 'arrival' : u"17:26" , 'departure' : u"17:26",},
                {'name' : u"岡谷", 'arrival' : u"17:30" , 'departure' : u"17:31",},
                {'name' : u"下諏訪", 'arrival' : u"17:35" , 'departure' : u"17:35",},
                {'name' : u"上諏訪", 'arrival' : u"17:40" , 'departure' : None,},
                ]
            }
        

    def tearDown(self) :
        self.db.close()
        os.unlink(self.dbFilename)


    def addStation(self, attr) :
        return self.db.addTimetableAttr(attr['company'],
                                        attr['railway_line'],
                                        attr['updown'],
                                        attr['day'],
                                        attr['direction'])


    def testAddStation(self) :
        timetableId = self.addStation(self.attr[0])
        self.assertEqual(timetableId, 0,"failed EkikaraDb.addTimetableAttr")
        self.assertEqual(self.db.getTimetableId(self.attr[0]['company'],
                                                self.attr[0]['railway_line'],
                                                self.attr[0]['updown'],
                                                self.attr[0]['day']),
                         0, "failed EkikaraDb.getTimetableId")


    def genTimetable(self, timetableId) :
        return ekikaradb.Timetable(db=self.db,
                                   target_timetable_id=timetableId,
                                   logger=self.logger)


    def testGenTimetable(self) :
        timetableId = self.addStation(self.attr[0])
        target = self.genTimetable(timetableId)
        attr = target.getAttr()
        self.assertEqual(attr['company'], self.attr[0]['company'], "failed Timetable.getAttr() company")
        self.assertEqual(attr['railway_line'], self.attr[0]['railway_line'], "failed Timetable.getAttr() railway_line")
        self.assertEqual(attr['updown'], self.attr[0]['updown'], "failed Timetable.getAttr() updown")
        self.assertEqual(attr['day'], self.attr[0]['day'], "failed Timetable.getAttr() day")


    def testSetLineStation(self) :
        timetableId = self.addStation(self.attr[0])
        target = self.genTimetable(timetableId)
        target.setLineStation(self.stationList1)
        i = -1
        for station in target.lineStation() :
            i += 1
            self.assertEqual(station['station_name'],
                             self.stationList1[i]['name'],
                             "failed target.lineStation() name[%d]"%i)
            self.assertEqual(station['display_arrival'],
                             self.stationList1[i]['arrival'],
                             "failed target.lineStation() arrival[%d]"%i)
            self.assertEqual(station['display_departure'],
                             self.stationList1[i]['departure'],
                             "failed target.lineStation() departure[%d]"%i)


    def testSetLineStationReverse(self) :
        idDown = self.addStation(self.attr[0])
        downTarget = self.genTimetable(idDown)
        downTarget.setLineStation(self.stationList1)
        idUp = self.addStation(self.attr[1])
        upTarget = self.genTimetable(idUp)
        upTarget.setLineStation(self.stationReverseList1)
        cursorDown = downTarget.lineStation()
        cursorUp = upTarget.lineStationReverse()
        self.assertNotEqual(cursorDown, None)
        self.assertNotEqual(cursorUp, None)
        rowDown = cursorDown.fetchone()
        rowUp = cursorUp.fetchone()
        while rowDown and rowUp :
            self.assertEqual(rowDown['station_name'], rowUp['station_name'], "different station name")
            rowDown = cursorDown.fetchone()
            rowUp = cursorUp.fetchone()


    def testOffsetStationOrder(self) :
        timetableId = self.addStation(self.attr[0])
        target = self.genTimetable(timetableId)
        target.setLineStation(self.stationList1)
        firstStationName = target.getFirstStation()
        self.assertEqual(firstStationName, self.stationList1[0]['name'], "failed Timetable.getFirstStation()")
        self.assertEqual(target.stationOrderMax(), len(self.stationList1)-1, "failede Timetaqble.stationOrderMax()")
        target.offsetStationOrder(100)
        self.assertEqual(target.stationOrderMax(),
                         len(self.stationList1)-1+100,
                         "failede Timetaqble.offsetStationOrder(100)")
        target.offsetStationOrder(-100)
        self.assertEqual(target.stationOrderMax(),
                         len(self.stationList1)-1,
                         "failede Timetaqble.offsetStationOrder(-100)")


    def testMergetStationList(self) :
        timetableId = self.addStation(self.attr[0])
        target = self.genTimetable(timetableId)
        target.setLineStation(self.stationList1)
        target.setLineStation(self.stationList2)
        i = -1
        for station in target.lineStation() :
            i += 1
            self.assertEqual(station['station_name'],
                             self.stationList2[i]['name'],
                             "failed target.lineStation() name[%d]"%i)


    def testMergetStationList(self) :
        timetableId = self.addStation(self.attr[0])
        target = self.genTimetable(timetableId)
        target.setLineStation(self.stationList1)
        target.setLineStation(self.stationList2)
        target.setLineStation(self.stationList3)
        targetStationList = self.stationList2 + self.stationList3[1:]
        i = -1
        for station in target.lineStation() :
            i += 1
            self.assertEqual(station['station_name'],
                             targetStationList[i]['name'],
                             "failed target.lineStation() name[%d]"%i)


    def getStationList(self, timetable) :
        stations = []
        for station in target.lineStation() :
            stations.append(station['station_name'])
        return stations

    def getTrainStationList(self, train) :
        stations = []
        for station in train['stations'] :
            stations.append(station['name'])
        return stations

    def testSetTrain(self):
        timetableId = self.addStation(self.attr[0])
        target = self.genTimetable(timetableId)
        target.setLineStation(self.stationList1)
        target.setLineStation(self.stationList2)
        target.setLineStation(self.stationList3)
        target.setTrain(timetableId, self.train1)
        trainStationNames = self.getTrainStationList(self.train1)
        for station in target.lineStation():
            arrival = None,
            departure = None,
            time = target.trainTime(self.train1['number'],
                                    self.train1['train_order'],
                                    station['station_name'])
            if station['station_name'] in trainStationNames :
                self.assertNotEqual(time,
                                    None,
                                    "exist station in train but return None value by target.trainTime()")
            else :
                self.assertEqual(time,
                                    None,
                                    "not exist station in train but return not-None value by target.trainTime()")


    def testPairTimetable(self) :
        id1 = self.addStation(self.attr[0])
        id2 = self.addStation(self.attr[1])
        self.assertNotEqual(id1, id2, "multiple timmetabale_attr set is failed")
        pair = self.db.pairTimetable(id1)
        self.assertEqual(pair['down'], id1, "failed pair down")
        self.assertEqual(pair['up'], id2, "failed pair up")
        pair = self.db.pairTimetable(id2)
        self.assertEqual(pair['down'], id1, "failed pair down")
        self.assertEqual(pair['up'], id2, "failed pair up")


    def testSetToOuDiaEki(self) :
        self.db.clearOuDiaEki()
        downId = self.addStation(self.attr[0])
        down= self.genTimetable(downId)
        down.setLineStation(self.stationList1)
        down.setToOuDiaEki()
        self.assertEqual(self.db.countOuDiaEki(), 6, "failed down.setToOuDiaEki()")
        for eki in self.db.listOuDiaEki() :
            i = eki['eki_order']
            self.assertEqual(eki['eki_name'], self.stationList1[i]['name'], "down only oudia_eki down failed name")
            self.assertEqual(eki['down_arrival'], self.stationList1[i]['arrival'], "down only oudia_eki down failed down_arrival")
            self.assertEqual(eki['down_departure'], self.stationList1[i]['departure'], "down only oudia_eki down failed down_departurel")
            self.assertEqual(eki['up_arrival'], None, "down only oudia_eki down failed up_arrival")
            self.assertEqual(eki['up_departure'], None, "down only oudia_eki down failed up_departurel")
        upId = self.addStation(self.attr[1])
        up = self.genTimetable(upId)
        up.setLineStation(self.stationReverseList1)
        up.setToOuDiaEki()
        self.assertEqual(self.db.countOuDiaEki(), 6, "failed down.setToOuDiaEki()(2nd)")
        for eki in self.db.listOuDiaEki() :
            i = eki['eki_order']
            self.assertEqual(eki['eki_name'], self.stationList1[i]['name'], "updown oudia_eki down failed name")
            self.assertEqual(eki['down_arrival'], self.stationList1[i]['arrival'], "updown oudia_eki down failed down_arrival")
            self.assertEqual(eki['down_departure'], self.stationList1[i]['departure'], "updown oudia_eki down failed down_departurel")
            ri = len(self.stationReverseList1) - 1 - i
            #PP.pprint(eki)
            #PP.pprint(self.stationReverseList1[ri])
            self.assert_(eki['up_arrival'] == 1 if
                         self.stationReverseList1[ri]['arrival'] == True 
                         else eki['up_arrival'] == 0 or eki['up_arrival'] is None,
                         "updown oudia_eki down failed up_arrival i=[%d],ri=[%d]"%(i,ri))
            self.assert_(eki['up_departure'] == 1 if
                         self.stationReverseList1[ri]['departure'] == True 
                         else eki['up_departure'] == 0 or eki['up_departure'] is None,
                         "updown oudia_eki down failed up_departure i=[%d],ri=[%d]"%(i,ri))

    def test_ekiTimeCursor(self):
        train1_cursor = self.db.conn.execute(ekikaradb.SELECT_DOWN_EKI_TIME_SQL,
                                             (0,
                                              u"519M",
                                              1))
        train1_time_list = [ station for station in train1_cursor ]
        timetableId = self.addStation(self.attr[0])
        target = self.genTimetable(timetableId)
        cursor_time_list = [ t 
                             for t in target.ekiTimeCursor(train_number=u"519M",
                                                           train_order=1) ]
        assert train1_time_list == cursor_time_list        

    # def test_trainList(self):
    #     assert False

    # def test_trainDateNoteList(self):
    #     assert False

    # def test_trainDateList(self):
    #     assert False

    # def test_setTrueTrainStop(self):
    #     assert False    

if __name__ == "__main__" :
    unittest.main()

#!/usr/bin/env python
# -*- coding:utf-8 -*-
u"""指定URLのページに続く全ページ掲載の列車詳細をスクレイピング

概要
----

ParseEkikaraに、えきから時刻表→路線検索→表形式のページのURLを与える。

ParseEkikaraはこのURLから当該ページを読み取り、以下の情報を得る。

- 全ページ取得
- 全ページの列車詳細ページ取得
- 列車詳細ページより列車・全停車駅名、停車駅の着時刻発時刻の取得
"""
import sys
reload(sys)
sys.setdefaultencoding('utf-8')  # for BeautifulSoup ?
import BeautifulSoup
import locale
import re
import urlparse
import types
import pprint
import traingrade
import pageopener
PASS_STRING = u"レ"
REPEAT_STRING = u"〃"
TRAIN_HEADERS = {
    u"列車名": 'name',
    u"列車番号": 'number',
    u"列車予約コード": 'resaave',
    u"連結車両": 'car',
    u"備考": 'note',
    u"運転日": 'date',
}
RE_PASS = re.compile(u"レ")
# 発時刻取得
# 臨時対応 (HH:MM)発
RE_DEPARTURE = re.compile(u"([0-9][0-9]:[0-9][0-9])\)?発", re.U)
# 着時刻取得
# 臨時対応 (HH:MM)着
reArrive = re.compile(u"([0-9][0-9]:[0-9][0-9])\)?着", re.U)
rePage = re.compile(u"[0-9]+頁", re.U)
reHeader = re.compile(u"\[(\S+)\u301c(\S+)\]\uff1a\s*(\S+)", re.U)
reGou = re.compile(u"(\D*)(\d+)号", re.U)
# re.sub用全角半角スペース
reSpace = re.compile(u"[　 ]|\\s", re.U)
# re.sub用全角コロン
reColon = re.compile(u"：", re.U)
# ディズニーリゾートライン&nbsp;リゾートゲートウェイ方面&nbsp;休日&nbsp;1/14頁
reNowPage = re.compile(u"\d+/\d+頁")
reTitle = re.compile(u"\[(\S+)\](\S+)&nbsp;"
                     u"(\S+)方面[(（](\S+)[)）]&nbsp;"
                     u"(\S+)&nbsp;\d+/\d+頁")
# [神戸新交通]ポートアイランド線&nbsp;
# 三宮方面(北埠頭経由)(上り)&nbsp;平日&nbsp;1/10頁
reTitleVia = re.compile(u"\[(\S+)\](\S+)&nbsp;"
                        u"(\S+)方面[(（](\S+)[)）][(（](\S+)[)）]&nbsp;"
                        u"(\S+)&nbsp;\d+/\d+頁")
reTitleNoUpdown = re.compile(u"\[(\S+)\](\S+)&nbsp;"
                             u"(\S+)方面&nbsp;(\S+)&nbsp;\d+/\d+頁")
reSmallTitle = re.compile(u"(\S+)&nbsp;(\S+)方面[(（](\S+)[)）]&nbsp;"
                          u"(\S+)&nbsp;\d+/\d+頁")
reSmallTitleNoUpdown = re.compile(u"(\S+)&nbsp;(\S+)方面&nbsp;"
                                  u"(\S+)&nbsp;\d+/\d+頁")

PRETTY = pprint.PrettyPrinter(indent=2)

def _is_tag_a_station(tag):
    u"""aタグかつhrefにstationを含むかどうか"""
    return (tag.name == 'a' and
            'station' in tag.get('href'))

def _is_tag_a_ekijikoku(tag):
    u"""aタグかつhrefにekijikokuを含むかどうか"""
    return (tag.name == 'a' and
            'ekijikoku' in tag.get('href'))

def _is_tag_span_textbold(tag):
    u"""spanタグかつclassにtextboldを含むかどうか"""
    return (tag.name == 'span' and
            (tag.get('class') and
             'textBold' in tag.get('class')))

def _is_tag_span_l(tag):
    u"""spanタグかつclassが'l'であるかどうか"""
    return (tag.name == 'span' and
            tag.get('class') == 'l')

def getHeaderText(tdBlock):
    u"""tdブロック内に含まれる文字列をリストにして返す
    文字列が存在する場合だけリストに含める(空文字列も含む)
    """
    return [reSpace.sub(u'', item.string)
            for item in tdBlock.contents
            if item.string and item.string != u""]

def getHeaderValues(tdBlock):
    u"""指定のtdブロックをfrom～to:valueを一組とするリストに分解

    戻り値
    ------

    リスト

    [{from:<<from>>, to:<<to>>, value:<<value>>},
    {from:<<from>>, to:<<to>>, value:<<value>>}, ...]
    """
    values = []
    for item in tdBlock.contents:
        if item.string:
            m = reHeader.search(item.string.strip())
            if m:
                values.append({'from': m.group(1),
                               'to': m.group(2),
                               'value': m.group(3) })
    return values

def infoStations(stations):
    u"""ステータスメッセージ用駅名文字列のリスト生成

    戻り値
    ------

    [<<駅名>>[ad], <<駅名>>[ad], ... ]

    a
      時刻表上で到着時刻表示があるとき

    b
      時刻表上で発時刻表示があるとき
    """ 
    def _add_flag(station):
        u"""到着時刻があればa,発時刻があればdを孵化する"""
        msg = station['name']
        if 'arrival' in station and station['arrival']:
            msg += u"a"
        if 'departure' in station and station['departure']:
            msg += u"d"
        return msg
    return [_add_flag(station) for station in stations]

def splitItemToStr(items):
    u"""itemsを単一文字列にして返す"""
    return u" ".join([u"%s〜%s：%s" % (item['from'],
                                       item['to'],
                                       item['value'])
                       for item in items])

class ParseEkikara(object):
    u"""えきから時刻表の指定の路線をスクレイピング"""
    def __init__(self,
                 logger,
                 status_option,
                 console_encode=locale.getpreferredencoding(),
                 is_sep_train=True):
        self.logger = logger       # logging object
        self.console_encode = console_encode
        self.is_sep_train = is_sep_train
        self.status_option = status_option
        self.grade = traingrade.TrainGrade(logger)

    def _statusMsg(self, s):
        u""""コンソールにステータスメッセージを出力"""
        sys.stderr.write(s.encode(self.console_encode))

    def getStationList(self, pageOpener, url, updown='down'):
        u"""urlで指定した表形式のページより駅名のリストをスクレイピング

        2011/01/08 by H.Haga
          路線の分岐に対応(但しリストは分岐を考慮せずのべたんで出力)

        2011/03/31 by H.Haga
          環状線等で同じ駅名があった場合でも
          頓着せずにそのまま出力するようにする

        2011/08/10 by H.Haga
          上り時刻表から得られた駅名リストは反転して返すようにした。
        """
        stations = []
        nameMap = []       # 直近の駅名の index 位置
        index = 0
        html = pageOpener.open(fullurl=url)
        soup = BeautifulSoup.BeautifulSoup(html)
        # get station list
        startPoint = soup.find(_is_tag_a_station)
        del soup
        if startPoint is not None:
            # aタグを含む tr タグを取得
            stationBlock = startPoint.parent.parent.parent.parent
            while stationBlock:
                item = stationBlock.find(_is_tag_a_station)
                if item is not None:
                    while item is not None:
                        station = {}
                        if (item.__class__.__name__ == 'Tag'
                            and item.name == u'a'):
                            station['name'] = item.string
                            self.logger.debug(u"[%s]" % (station['name'],))
                            stations.append(station)
                            nameMap.append(len(stations) - 1)
                        elif (item.__class__.__name__ == 'NavigableString' and
                              item.string.strip() == REPEAT_STRING):
                            nameMap.append(len(stations) - 1)
                        item = item.nextSibling
                    aBlock = stationBlock.find(_is_tag_a_ekijikoku)
                    if aBlock is not None:
                        element = aBlock.parent.parent.next
                        while element is not None:
                            if (element.__class__.__name__ == 'Tag'
                                and element.name == 'span'):
                                if element.string:
                                    stations[nameMap[index]]['arrival'] = True
                                    self.logger.debug(
                                        u"staitons[%d]=[%s] arrival true" %
                                        (nameMap[index],
                                         stations[nameMap[index]]['name']))
                                else:
                                    stations[nameMap[index]]['departure'] = True
                                    self.logger.debug(
                                        u"staitons[%d]=[%s] departure true" %
                                        (nameMap[index],
                                         stations[nameMap[index]]['name']))
                                index += 1
                            element = element.findNextSibling('span')
                if stationBlock is not None:
                    # 次のtrタグに駅名が無いか調査するため。
                    stationBlock = stationBlock.findNextSibling('tr')
        if updown == 'up':
            # 上り時刻表から得られた駅名リストは反転する。
            stations.reverse()
        msgStations = (u"parsed stations:" +
                       u",".join(infoStations(stations)))
        self.logger.debug(msgStations)
        if self.status_option:
            self._statusMsg(msgStations + u"\n")
        return stations

    def getTimeTableAttr(self, pageOpener, url):
        u"""時刻表ページより路線のデータをスクレイピング

        戻り値
        ------

        辞書形式で戻る

        company
          (あれば)鉄道会社名

        line
          線名

        updown
          上り下りを示す文字列。北行や南行、東行や西行など各種。
          ここではスクレイピング結果がそのまま入る。
          後で変換処理して'up' or 'down'に変換。

        day
          平日、土曜日、日祭日等
        """
        attr = {}
        html = pageOpener.open(fullurl=url)
        soup = BeautifulSoup.BeautifulSoup(html)
        spans = soup.findAll(_is_tag_span_textbold)
        del soup
        for span in spans:
            # [名古屋市営]名城線&nbsp;
            # ナゴヤドーム前矢田方面（左回り）&nbsp;平日&nbsp;1/16頁
            # [神戸新交通]ポートアイランド線&nbsp;
            # 三宮方面(北埠頭経由)(上り)&nbsp;平日&nbsp;1/10頁
            if span.string and reNowPage.search(span.string.strip()):
                mTitleVia = reTitleVia.search(span.string.strip())
                # reTitleにもマッチしてしまうのでreTitleViaを先にチェック
                if mTitleVia:
                    attr['company'] = mTitleVia.group(1)
                    attr['line'] = mTitleVia.group(2)
                    attr['direction'] = (mTitleVia.group(3) +
                                         mTitleVia.group(4))
                    attr['updown'] = mTitleVia.group(5)
                    attr['day'] = mTitleVia.group(6)
                    continue
                mTitle = reTitle.search(span.string.strip())
                if mTitle:
                    attr['company'] = mTitle.group(1)
                    attr['line'] = mTitle.group(2)
                    attr['direction'] = mTitle.group(3)
                    attr['updown'] = mTitle.group(4)
                    attr['day'] = mTitle.group(5)
                    continue
                mSmallTitle = reSmallTitle.search(span.string.strip())
                if mSmallTitle:
                    attr['company'] = mSmallTitle.group(1) 
                    attr['line'] = u""
                    attr['direction'] = mSmallTitle.group(2)
                    attr['updown'] = mSmallTitle.group(3)
                    attr['day'] = mSmallTitle.group(4)
                    continue
                # 2011/01/23 by kuma35
                # 方面の後に(上り)等のカッコ無しパターン.
                # 例
                #  [伊予鉄道]３系統(市駅線)
                #  道後温泉方面 平日 1/9頁
                mTitleNoUpdown = reTitleNoUpdown.search(span.string.strip())
                if mTitleNoUpdown:
                    attr['company'] = mTitleNoUpdown.group(1)
                    attr['line'] = mTitleNoUpdown.group(2)
                    attr['direction'] = mTitleNoUpdown.group(3)
                    # updownにdirectionをそのままセットしておく
                    attr['updown'] = attr['direction']
                    attr['day'] = mTitleNoUpdown.group(4)
                    continue
                # 2011/08/06 for ディズニーリゾートライン。
                mSmallTitleNoUpdown = reSmallTitleNoUpdown.search(
                    span.string.strip())
                if mSmallTitleNoUpdown:
                    attr['company'] = mSmallTitleNoUpdown.group(1)
                    attr['line'] = u""
                    attr['direction'] = mSmallTitleNoUpdown.group(2)
                    # updownにdirectionをそのままセットしておく
                    attr['updown'] = attr['direction']
                    attr['day'] = mSmallTitleNoUpdown.group(3)
                    continue
        return attr

    def getPages(self, pageOpener, url):
        u"""urlで指定した表形式ページから続くページの全urlを取得

        urlで指定した表形式ページから続くページの全urlを取得する。
        相対urlは絶対pathに変換して返す。

        戻り値
        ------

        [fulll-path-url,full-path-url...]

        """
        def _get_page_url(url, option_block):
            u"""optionタグから路線時刻表を示すページのURLを取得"""
            page_url = urlparse.urljoin(url,
                                        option_block.get('value') + ".htm")
            self.logger.debug(u"page_url=[%s]" % (page_url,))
            return page_url
        # own page and other pages
        html = pageOpener.open(fullurl=url)
        soup = BeautifulSoup.BeautifulSoup(html)
        pages = [_get_page_url(url, option_block)
                 for option_block in soup.findAll(lambda tag:
                                                      tag.name == 'option')
                 if option_block.string and rePage.search(option_block.string)]
        self.logger.debug(PRETTY.pformat(pages))
        msgCount = u"page url count=[%d]" % (len(pages),)
        self.logger.debug(msgCount)
        if self.status_option:
            self._statusMsg(msgCount + u"\n")
        return pages

    def getDetails(self, pageOpener, url):
        u"""url指定の表形式ページより、そのページの列車詳細へのリンクを全取得

        url指定の表形式ページより、そのページの列車詳細へのリンクを全取得。
        重複を省くため set() して返す
        """
        details = []
        detailsSet = set(details)
        for tablePageUrl in self.getPages(pageOpener, url):
            html = pageOpener.open(fullurl=tablePageUrl)
            soup = BeautifulSoup.BeautifulSoup(html)
            for detailUrl in soup.findAll(lambda tag: tag.name == 'a' and
                                          'detail' in tag.get('href') and
                                          u"詳細" in tag.contents):
                detailPageUrl = urlparse.urljoin(tablePageUrl,
                                                 detailUrl.get('href'))
                self.logger.debug(u"detailUrl=[%s]" % (detailPageUrl,))
                if not detailPageUrl in detailsSet:
                    details.append(detailPageUrl)
                    #detailSet = set(details)
            if self.status_option:
                self._statusMsg(u"[%d]" % (len(details),))
        self.logger.debug(PRETTY.pformat(details))
        if self.status_option:
            self._statusMsg(u"\ndetail page url total count=[%d]\n" %
                              (len(details),))
        return details

    def getTrainHeader(self, tdBlocks):
        u"""列車詳細ページにて時刻以外のデータを取得

        引数
        ----

        列車詳細ページにてヘッダを含むtdタグブロック。
        

        戻り値
        ------

        ヘッダ情報を含む辞書
        """
        # number が types.ListType でなくても、
        # 他のパラメータが ListType の可能性がある hoge～fuga：.... 形式の時
        # number が正義とする(2010/09/14)
        train = {}
        strTitleColumn = None
        for titleColumn in tdBlocks[0].findAll(_is_tag_span_textbold):
            if titleColumn.string:
                strTitleColumn = titleColumn.string.strip()
                self.logger.debug(u"getTrainHeader strTitleColumn=[%s]" %
                                  (strTitleColumn,))
                if strTitleColumn in TRAIN_HEADERS:
                    for titleValue in tdBlocks[1].findAll(_is_tag_span_l):
                        if titleValue.string:
                            train[TRAIN_HEADERS[strTitleColumn]] = (
                                titleValue.string.strip())
                            continue
                        if self.is_sep_train:
                            headers = getHeaderValues(titleValue)
                            if  headers:
                                train[TRAIN_HEADERS[strTitleColumn]] = headers
                            else:
                                # 複数行(複数タグ)を単一文字列にする。
                                train[TRAIN_HEADERS[strTitleColumn]] = (
                                    u"|".join(getHeaderText(titleValue)))
                        else:
                            headers = getHeaderValues(titleValue)
                            if  headers:
                                # 成形する
                                formes = [u"%s(%s～%s)" %
                                          (header['value'],
                                           header['from'],
                                           header['to'])
                                          for header in headers ]
                                train[TRAIN_HEADERS[strTitleColumn]] = (
                                    u"|".join(formes))
                            else:
                                # 複数行(複数タグ)を単一文字列にする。
                                train[TRAIN_HEADERS[strTitleColumn]] = (
                                    u"|".join(getHeaderText(titleValue)))
        return train

    def getTrainTime(self, tdBlocks):
        u"""tdブロックに掲載の時刻を取得

        戻り値
        ------

        [{'name': <<駅名>>,
        'arrival':<<到着時刻>>または通過記号レ,
        'departure':<<発時刻>>または通過記号レ,
        'platform':<<番線>>または空文字列,} ... ]

        """
        station = {}
        station_order = 0
        for stationName in tdBlocks[0].findAll(_is_tag_a_station):
            station = {'name': None,
                       'arrival': None,
                       'departure': None,
                       'platform': None}
            if stationName.string:
                station_order += 1
                station['order'] = station_order
                station['name'] = stationName.string
                for stationTime in tdBlocks[1].findAll(_is_tag_span_l):
                    if stationTime.string:
                        matchPass = RE_PASS.search(stationTime.string.strip())
                        if  matchPass:
                            station['arrival'] = PASS_STRING
                            station['departure'] = PASS_STRING
                        else:
                            matchArrive = (
                                reArrive.search(stationTime.string.strip()))
                            if matchArrive:
                                station['arrival'] = matchArrive.group(1)
                            else:
                                matchDeparture = (
                                    RE_DEPARTURE.search(
                                        stationTime.string.strip()))
                                if matchDeparture:
                                    station['departure'] = (
                                        matchDeparture.group(1))
                                else:
                                    pass
                for platform in tdBlocks[2].findAll(_is_tag_span_l):
                    if platform.string:
                        if (platform.string.strip() == u"&nbsp;" or
                            platform.string.strip() == u""):
                            station['platform'] = u""
                        else:
                            station['platform'] = platform.string.strip()
            return station

    def getTrain(self, pageOpener, url):
        u"""列車詳細のurlよりその列車の情報を取得
        
        戻り値
        ------

        戻り値は辞書です。辞書の中身は下記。

        stations
          着時刻・発時刻の駅純リスト。詳細は getTrainTime 参照。

        url
          引数で渡されたurlそのもの。通常ダイヤと年末年始ダイヤを
          同じページに仕込んで来たことがあり、列車番号だけでは
          区別できないことがあるので識別子として列車詳細ページの
          URLをそのままキーとして持つようにしている。


        ヘッダ情報
        ---------

        getTrainHeaderが返す以下の情報は単一の文字列またはリストの事がある。

        name
          列車名

        number
          列車番号

        resaave
          列車予約コード

        car
          連結車両

        note
          備考

        date
          運転日。日付だけでなく、運転日に関するテキストが入る事がある。
        """
        def _debug_message(number, name):
            strMsg = u"\nparsed"
            if isinstance(number, types.StringTypes):
                strMsg += u"[%s]" % (number,)
            elif isinstance(number, types.ListType):
                strMsg += u"["
                for item in number:
                    strMsg += (u"[%s]～[%s]:[%s]," %
                               (item['from'], item['to'], item['value'],))
                strMsg += u"]"
            if isinstance(name, types.StringTypes):
                strMsg += u"[%s]" % (name,)
            elif isinstance(name, types.ListType):
                strMsg += u"["
                for item in name:
                    strMsg += (u"[%s]～[%s]:[%s]," %
                               (item['from'], item['to'], item['value'],))
                strMsg += u"]"
            return strMsg
        train = {'stations': [], 'url': url}
        html = pageOpener.open(fullurl=url)
        soup = BeautifulSoup.BeautifulSoup(html)
        for divBlock in soup.findAll(name='div', attrs={'id': 'container02'}):
            for trBlock in divBlock.findAll('tr'):
                tdBlocks = trBlock.findAll('td')
                if  len(tdBlocks) == 2:
                    header = self.getTrainHeader(tdBlocks)
                    if header:
                        train.update(header)
                elif len(tdBlocks) == 3:
                    detail = self.getTrainTime(tdBlocks)
                    if detail:
                        train['stations'].append(detail)
        self.logger.debug(PRETTY.pformat(train))
        if self.status_option and TRAIN_HEADERS[u"列車番号"] in train:
            number = train[TRAIN_HEADERS[u"列車番号"]]
            name = train[TRAIN_HEADERS[u"列車名"]]
            self._statusMsg(_debug_message(number, name))
        return train

    def splitTrainItem(self, numbers, items):
        u"""item(s)を返す。numberと並びが違う時は同一文字列のリストを返す

        phase1.numberと違う時は、単一文字列に合成した上で、
        その単一文字列を値に持つリストを返す。
        (phase2.numberと違う時は、単なる省略であれば分割する。無理なときは上記)
        """
        result = []
        if isinstance(items, types.ListType):
            if len(numbers) == len(items):
                def _item_paired(number, item):
                    if (number['from'] == item['from'] and
                        number['to'] == item['to']):
                        return {'from': item['from'],
                                'to': item['to'],
                                'value': item['value']}
                    else:
                        return {'from': number['from'],
                                'to': number['to'],
                                'value': splitItemToStr(items)}
                result += [_item_paired(number, item)
                           for number, item in zip(numbers, items)]
            else:
                result += [{'from': number['from'],
                            'to': number['to'],
                            'value': splitItemToStr(items)}
                            for number in numbers]
        else:
            result += [{'from': numbers[i]['from'],
                        'to': numbers[i]['to'],
                        'value': items}
                       for i in range(len(numbers))]
        return result

    def splitItem(self, numbers, items):
        u"""item(s)を返す。numberと並びが違う時は同一文字列のリストを返す

        分割ルール
        ----------

        # numberと並びが一緒の場合はitemsをそのまま返す。
        # numberと並びが違う時は、itemを単一文字列に合成した上で、
          その単一文字列を値に持つリストを返す。
        (phase2.numberと違う時は、単なる省略であれば分割する。無理なときは上記)
        """
        def _equal_range(numbers, items):
            for number, item in zip(numbers, items):
                if number['from'] != item['from'] or number['to'] != item['to']:
                    return False
            return True
        if isinstance(items, list):
            if len(numbers) == len(items):
                if _equal_range(numbers, items):
                    # numbers と items の範囲指定は同一
                    return [{'from': number['from'],
                             'to': number['to'],
                             'value': item['value']}
                            for number, item in zip(numbers, items)]
                else:
                    # numbers と items の範囲指定は違う
                    # →numbersに合わせる
                    #   itemsは単一文字列化する。
                    value = splitItemToStr(items)
                    return [{'from': number['from'],
                             'to': number['to'],
                             'value': value}
                            for number in numbers]
            else:
                # numbers と items の範囲指定は違う
                # →numbersに合わせる
                #   itemsは単一文字列化する
                value = splitItemToStr(items)
                return [{'from': number['from'],
                         'to': number['to'],
                         'value': value}
                        for number in numbers]
        else:
            # itemsは単一文字列
            return [{'from': number['from'],
                     'to': number['to'],
                     'value': items}
                    for number in numbers]

    def splitTrain(self, train):
        u"""単一の列車に分割する。列車番号 or 列車名がListTypeの場合に呼ぶ。

        列車番号(train['number']) or 列車名(train['name'])のいずれかまたは
        両方がListTypeの場合に呼び出し、単一の列車に分割したリストを返す。

        引数
        ----
        
        train
          スクレイピングした列車詳細データ

        戻り値
        ------

        [single, single, ... ]
          分割した列車詳細データのリスト
        
        """
        # number とそろっているかどうか確認し、そろってないものは
        # 0.大原則: numberが正義とする
        # (1. 一部省略であり、展開可能なものは展開する。)
        # 2. number と互換が無い場合はlistをstringにする。
        #    互換の無い場合で hoge〜fuga:tee 形式で無いものは
        #     getHeaderText にて既に文字列になっているので、
        #    ここでは hoge〜fuga:tee 形式かつnumberと互換の無いものが対象。
        stationMap = {}
        for i, v in enumerate(train['stations']):
            stationMap[v['name']] = i
        if isinstance(train['number'], list):
            # train['number'] is list かつ train['name'] is list or not list
            trains = []
            for (number, name, date, car, note
                 ) in zip(train['number'], 
                          self.splitItem(train['number'], train['name']),
                          self.splitItem(train['number'], train['date']),
                          self.splitItem(train['number'], train['car']),
                          self.splitItem(train['number'], train['note'])):
                start = stationMap[number['from']]
                end = stationMap[number['to']]
                trains.append({'number': number['value'],
                               'name': name['value'],
                               'stations': train['stations'][start: end + 1],
                               'note': note['value'],
                               'date': date['value'],
                               'car': car['value'],
                               'url': train['url']})
            return trains
        elif isinstance(train['name'], list):
            trains = []
            for (number, name, date, car, note
                 ) in zip(self.splitItem(train['name'], train['number']),
                          train['name'],
                          self.splitItem(train['name'], train['date']),
                          self.splitItem(train['name'], train['car']),
                          self.splitItem(train['name'], train['note'])):
                start = stationMap[number['from']]
                end = stationMap[number['to']]
                trains.append({'number': number['value'],
                               'name': name['value'],
                               'stations': train['stations'][start: end + 1],
                               'note': note['value'],
                               'date': date['value'],
                               'car': car['value'],
                               'url': train['url']})
            return trains
        else:
            # 両方ともlistじゃなかった。
            # train 1つだけをlistに入れて返す
            return [train]

    def extractTrainName(self, name):
        u"""列車名を種別、名前、号に分解
        
        戻り値
        ------

        type
          特急、快速、普通 etc.の種別

        type_prefix
          特別 など、typeを修飾する単語。 例：特別快速、通勤快速 など

        name
          type, type_prefix, gou を除いた残り

        gou
          末尾が[\d+号]の場合、\d+ を取得。
        """
        item = {'type': None, 'type_prefix': None, 'name': None, 'gou': None}
        name = name.strip()
        mType = self.grade.re_type.search(name)
        if mType:
            item['type_prefix'] = mType.group(1)
            item['type'] = mType.group(2)
            name = mType.group(3)
        mGou = reGou.search(name)
        if mGou:
            item['gou'] = mGou.group(2)
            name = mGou.group(1)
        item['name'] = name
        return item


# --------------- test
if __name__ == '__main__':
    import gettimetablelog
    from optparse import OptionParser
    optParser = OptionParser(usage="%prog [options] URL [ URL... ]",
                             version="%prog 0.2")
    optParser.add_option("-p", "--proxy",
                         dest="proxy",
                         help=("specify proxy if you need. "
                               "less scheme name(NG; "
                               "http://hogehoge.com:8080, "
                               "GOOD; hogehoge.com:8080)"),
                         metavar="URL")
    optParser.add_option("-s", "--status", action="store_true",
                         dest="status", help="status messages to stderr")
    (OPTIONS, args) = optParser.parse_args()
    logger = gettimetablelog.logging.getLogger("gettimetable")
    CONSOLE_ENCODE = locale.getpreferredencoding()
    pageOpener = pageopener.PageOpener(logger=logger,
                                       proxy=OPTIONS.proxy,
                                       console_encode=CONSOLE_ENCODE)
    parser = ParseEkikara(logger=logger,
                          status_option=OPTIONS.status,
                          console_encode=CONSOLE_ENCODE)
    train_order = 0
    for url in args:
        stationList = parser.getStationList(pageOpener, url)
        for detailUrl in parser.getDetails(pageOpener, url):
            train = parser.getTrain(pageOpener, detailUrl)
            for singleTrain in parser.splitTrain(train):
                singleTrain.update(
                    parser.extractTrainName(singleTrain['name']))
                train_order += 1
